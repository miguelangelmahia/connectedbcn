<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Peticiones</title>
    <style>
        body {
            background-color: beige;
        }
    </style>
</head>

<body onload="get_codigo_linea()">


    <div class="container">
        <div class="row">
            <div class="col 12">
                <h1>https://api.tmb.cat/v1/transit/linies/metro/{codi_linia}/estacions</h1>
            </div>
        </div>
        <div class="row">
            <div class="col 6">
                <form>
                    <div class="form-group col-md-6">
                        <label for="inputState">Linea de Metro</label>
                        <select id="inputState" class="form-control">
                            
                        </select>
                    </div>
                </form>
            </div>
            <div class="col 6">
                <a href="index.jsp">atras</a>
            </div>
        </div>
        <div class="row">
            <div class="col 12">
                <table class="table table-dark">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nombre de estacion</th>
                            <th scope="col">Nombre de linia</th>
                            <th scope="col">Estado</th>
                            <th scope="col">Grup estacion</th>
                           
                        </tr>
                    </thead>
                    <tbody id="lista-tablas"></tbody>
                </table>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <script>
    

         var id =[];
        function get_codigo_linea() {
            $.getJSON('https://api.tmb.cat/v1/transit/linies/metro?app_id=fe45ede7&app_key=71e88ac0e394d61b622c34d29a624b72',
                function (respuesta) {
                    var c = respuesta.features;

                    // console.log(c)
                    var listaLinias = $("#inputState");

                    $.each(c, function (index, elemento) {

                        id.push(elemento.properties.CODI_LINIA);

                        listaLinias.append(
                            //<option id="elemento.properties.CODI_LINIA">elemento.properties.NOM_LINIA</option>
                            '<option id="selection" value="'+elemento.properties.CODI_LINIA+'">'+elemento.properties.NOM_LINIA+'</option>'
                        );
                        // console.log(index + " | " + elemento.properties.NOM_TIPUS_TRANSPORT + " ||" + elemento.properties.NOM_LINIA + " || " +
                        //     elemento.properties.CODI_LINIA + " || " + elemento.properties.ORIGEN_LINIA + " || " + elemento.properties.DESTI_LINIA)
                    });

                });
        }
        var code_linia;
        $("#inputState").change(function(){
        //    console.log("code: " + id[$(this)[0].selectedIndex])
           code_linia = id[$(this)[0].selectedIndex]
             console.log("CODI: " + code_linia)
             get_list()
        });
        // https://api.tmb.cat/v1/transit/linies/metro/{codi_linia}/estacions?app_id=fe45ede7&app_key=71e88ac0e394d61b622c34d29a624b72
        
        function get_list(){
             url = "https://api.tmb.cat/v1/transit/linies/metro/"+code_linia+"/estacions?app_id=fe45ede7&app_key=71e88ac0e394d61b622c34d29a624b72"
            $.getJSON(url,function(respuesta){
             var r = respuesta.features;
             $("#lista-tablas").text("");
             var tabla_escrita = $("#lista-tablas")
              $.each(r, function (index, elemento) {
                    tabla_escrita.append(
                             '<tr>'
                            + '<td>' + index + '</td>'
                            + '<td>' + elemento.properties.NOM_ESTACIO + '</td>'
                            + '<td>' + elemento.properties.NOM_LINIA + '</td>'
                            + '<td>' + elemento.properties.NOM_TIPUS_ESTAT + '</td>'
                            + '<td>' + elemento.properties.CODI_GRUP_ESTACIO + '</td>'
                            + '</tr>'
                    );
                    console.log(index + " || " + elemento.properties.NOM_ESTACIO + " || " + elemento.properties.NOM_LINIA + " || " + elemento.properties.NOM_TIPUS_ESTAT
                    + " || " + elemento.properties.CODI_GRUP_ESTACIO)
              });
                
            });
        }


    </script>
</body>

</html>