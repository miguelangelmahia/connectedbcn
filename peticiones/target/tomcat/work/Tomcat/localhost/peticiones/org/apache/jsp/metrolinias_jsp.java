/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/7.0.47
 * Generated at: 2019-04-10 14:11:40 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class metrolinias_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("<!DOCTYPE html>\r\n");
      out.write("<html lang=\"es\">\r\n");
      out.write("\r\n");
      out.write("<head>\r\n");
      out.write("    <meta charset=\"UTF-8\">\r\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n");
      out.write("    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\r\n");
      out.write("    <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\"\r\n");
      out.write("        integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\">\r\n");
      out.write("    <title>Peticiones</title>\r\n");
      out.write("    <style>\r\n");
      out.write("        body {\r\n");
      out.write("            background-color: beige;\r\n");
      out.write("        }\r\n");
      out.write("    </style>\r\n");
      out.write("</head>\r\n");
      out.write("\r\n");
      out.write("<body>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("    <div class=\"container\">\r\n");
      out.write("        <div class=\"row\">\r\n");
      out.write("            <div class=\"col 12\">\r\n");
      out.write("                <h1>https://api.tmb.cat/v1/transit/linies/metro</h1>\r\n");
      out.write("            </div>\r\n");
      out.write("        </div>\r\n");
      out.write("        <div class=\"row\">\r\n");
      out.write("            <div class=\"col 6\">\r\n");
      out.write("                <button class=\"btn btn-primary\" id=\"btn_pruebas\">Cargar...</button>\r\n");
      out.write("            </div>\r\n");
      out.write("            <div class=\"col 6\">\r\n");
      out.write("                <a href=\"index.jsp\">atras</a>\r\n");
      out.write("            </div>\r\n");
      out.write("        </div>\r\n");
      out.write("        <div class=\"row\">\r\n");
      out.write("            <div class=\"col 12\">\r\n");
      out.write("                <table class=\"table table-dark\">\r\n");
      out.write("                    <thead>\r\n");
      out.write("                        <tr>\r\n");
      out.write("                            <th scope=\"col\">#</th>\r\n");
      out.write("                            <th scope=\"col\">Transporte</th>\r\n");
      out.write("                            <th scope=\"col\">Nombre de linia</th>\r\n");
      out.write("                            <th scope=\"col\">Codigo de linia</th>\r\n");
      out.write("                            <th scope=\"col\">Origen</th>\r\n");
      out.write("                            <th scope=\"col\">Destino</th>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                    </thead>\r\n");
      out.write("                    <tbody id=\"lista-Linias\"></tbody>\r\n");
      out.write("                </table>\r\n");
      out.write("            </div>\r\n");
      out.write("        </div>\r\n");
      out.write("    </div>\r\n");
      out.write("\r\n");
      out.write("    <script src=\"https://code.jquery.com/jquery-3.3.1.min.js\"></script>\r\n");
      out.write("    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js\"\r\n");
      out.write("        integrity=\"sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1\"\r\n");
      out.write("        crossorigin=\"anonymous\"></script>\r\n");
      out.write("    <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js\"\r\n");
      out.write("        integrity=\"sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM\"\r\n");
      out.write("        crossorigin=\"anonymous\"></script>\r\n");
      out.write("    <script>\r\n");
      out.write("        $(\"#btn_pruebas\").on(\"click\", getUsers);\r\n");
      out.write("\r\n");
      out.write("        function getUsers() {\r\n");
      out.write("            $.getJSON('https://api.tmb.cat/v1/transit/linies/metro?app_id=fe45ede7&app_key=71e88ac0e394d61b622c34d29a624b72',\r\n");
      out.write("                function (respuesta) {\r\n");
      out.write("                    var c = respuesta.features;\r\n");
      out.write("\r\n");
      out.write("                    console.log(c)\r\n");
      out.write("                    var listaLinias = $(\"#lista-Linias\");\r\n");
      out.write("\r\n");
      out.write("                    $.each(c, function (index, elemento) {\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("                        listaLinias.append(\r\n");
      out.write("                            '<tr>'\r\n");
      out.write("                            + '<td>' + index + '</td>'\r\n");
      out.write("                            + '<td>' + elemento.properties.NOM_TIPUS_TRANSPORT + '</td>'\r\n");
      out.write("                            + '<td>' + elemento.properties.NOM_LINIA + '</td>'\r\n");
      out.write("                            + '<td>' + elemento.properties.CODI_LINIA + '</td>'\r\n");
      out.write("                            + '<td>' + elemento.properties.ORIGEN_LINIA + '</td>'\r\n");
      out.write("                            + '<td>' + elemento.properties.DESTI_LINIA + '</td>'\r\n");
      out.write("                            + '</tr>'\r\n");
      out.write("                        );\r\n");
      out.write("                        console.log(index + \" | \" + elemento.properties.NOM_TIPUS_TRANSPORT + \" ||\" + elemento.properties.NOM_LINIA + \" || \" +\r\n");
      out.write("                            elemento.properties.CODI_LINIA + \" || \" + elemento.properties.ORIGEN_LINIA + \" || \" + elemento.properties.DESTI_LINIA)\r\n");
      out.write("                    });\r\n");
      out.write("\r\n");
      out.write("                });\r\n");
      out.write("        }\r\n");
      out.write("    </script>\r\n");
      out.write("</body>\r\n");
      out.write("\r\n");
      out.write("</html>");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
