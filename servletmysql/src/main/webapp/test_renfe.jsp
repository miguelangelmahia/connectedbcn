<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.codesplai.servletmysql.*" %>
<%@page import="com.codesplai.peticiones.*" %>
<%@page import="com.codesplai.basedatos.*" %>
<%@page import="java.util.*" %>
<!DOCTYPE html>
<html lang="es">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Plantilla</title>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
	integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/estilosPlantilla.css">
</head>

<body>
	<nav class="navbar navbar-expand-lg menu">
		<a class="navbar-brand" href="#">ConnectedBCN</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<i class="fas fa-bars menu-comprimido"></i>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link link-menu"
					href="#">Inicio <span class="sr-only">(current)</span>
				</a></li>
				<li class="nav-item"><a class="nav-link link-menu" href="#">Bicing</a></li>
				<li class="nav-item"><a class="nav-link link-menu" href="#">Metro</a></li>
				<li class="nav-item"><a class="nav-link link-menu" href="#">TRAM</a></li>
				<li class="nav-item"><a class="nav-link link-menu" href="#">eCooltra</a>
				</li>
			</ul>
			<button class="boton-menu" type="submit">
				Entra <i class="fas fa-sign-in-alt"></i>
			</button>
			<button class="boton-menu" type="submit">
				Regístrate <i class="fas fa-user-plus"></i>
			</button>
		</div>
	</nav>
	<div class="container">
		<div class="row">
		    <div class="col 4">
                <table class="table pruebatabla">
                    <thead class="thead-dark">
                        <tr>
                            <th> Operador</th>
                            <th> Nombre</th>
                            <th> Linea</th>
                            <th> Tipo</th>
                            <th> Longitud</th>
                            <th> Latitud</th>
                        </tr>
                    </thead>
                    <%for(Renfe e : RenfeController.getAll()){%>
                        <tr>
                            <td>><%=e.operator%></td>
                            <td><%=e.name%></td>
                            <td><%=e.line%></td>
                            <td><%=e.type%></td>
                            <td><%=e.lon%></td>
                            <td><%=e.lat%></td>
                        </tr>
                    <%}%>
                </table>
            </div> 
		</div>
	</div>
	<footer class="container-fluid">
		<div class="row">
			<div class="col-12 redes-sociales">
				<div class="row d-flex justify-content-around">
					<div class="col-3 col-sm-2 d-flex justify-content-center">
						<div
							class="facebook d-flex justify-content-around align-items-center">
							<i class="fab fa-facebook-f"></i>
						</div>
					</div>
					<div class="col-3 col-sm-2 d-flex justify-content-center">
						<div
							class="google-plus d-flex justify-content-around align-items-center">
							<i class="fab fa-google-plus-g"></i>
						</div>
					</div>
					<div class="col-3 col-sm-2 d-flex justify-content-center">
						<div
							class="instagram d-flex justify-content-around align-items-center">
							<i class="fab fa-instagram"></i>
						</div>
					</div>
					<div class="col-3 col-sm-2 d-flex justify-content-center">
						<div
							class="twitter d-flex justify-content-around align-items-center">
							<i class="fab fa-twitter"></i>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12">
				<div class="row">
					<div class="col-12 col-md-6 text-center contacto text-center">
						<p>
							Ponte en contacto con nosotros pulsando <a href="#">aqu�</a>.
						</p>
					</div>
					<div class="col-12 col-md-6 text-center copyright text-center">
						<p>@Copyright 2019 ConnectedBCN</p>
					</div>
				</div>
			</div>
		</div>
	</footer>

	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
	
</body>

</html>