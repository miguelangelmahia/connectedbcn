<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.codesplai.servletmysql.*" %>
<%@page import="com.codesplai.peticiones.*" %>
<%@page import="com.codesplai.basedatos.*" %>
<%@page import="java.util.*" %>

<html>

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/estilos.css">

</head>
<%

 //List<Metro_lineas> lista =  Metro_lineasController.getAll();
 //List<Stationsbicing> lista1 =  StationsbicingController.getAll();
    //List<Bicibox> listado = BiciboxController.getAll();
        //List<Renfe>listarenfe= RenfeController.getAll();
       // List<TRAM_stops> listatramparadas = TramController.get_stops_info();

    StringBuilder sb = new StringBuilder();

     for(Metro_lineas m : Metro_lineasController.getAll()){

            sb.append("<tr>");

            sb.append("<td>");
            sb.append(m._nom_linia);
            sb.append("</td>");

            for(Estaciones e : m.estations){
                sb.append("<td>");
                sb.append(e._nom_estacio);
                sb.append("</td>");
            }

            sb.append("</tr>");

        }
    String filas = sb.toString();
    //TramController.get_apiKey_Tam();
    //KeytramController.validacion();
    //RenfeController.getAll();
    //TramController.get_stops_info();
    //KeytramController.tokenultimo();
    
%>

<body onlad="getLocation()">
    <h2>Hello World!</h2>
    <a href="gps.jsp">gps</a>
    <a href="http://localhost:8080/servletmysql/main">pica aki</a> -->


     <div class="container">
        <div class="row">
            <div class="col 4">
                <table class="table pruebatabla">
                    <thead class="thead-dark">
                        <tr>
                            <th>Nombre de Estacion</th>
                            <th>Direccion</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%=filas%>
                    </tbody>
                </table>
            </div> 
             <div class="col 4">
                <table class=" table pruebatabla">
                    <thead class="thead-dark">
                        <tr>
                            <th>Nombre</th>
                            <th> Espacios Disponibles</th>
                        </tr>
                    </thead>
                    <%for(Stationsbicing e : StationsbicingController.getAll()){%>
                    <tr>
                        <td><%=e.name%></td>
                        <td><%=e.empty_slots%></td>
                    </tr>
                    <%}%></table>
        </div> 
         <div class="col 4">
            <table class="table pruebatabla">
                <thead class="thead-dark"><tr>
                    <th>Direccion</th>
                    <th> Espacios disponibles</th>
                </tr></thead>
                <%for(Bicibox e : BiciboxController.getAll()){%>
                    <tr>
                        <td>><%=e.direccio%></td>
                        <td><%=e.buits%></td>
                    </tr>
                    <%}%>
            </table>
                    </div>
                <div class="col 4">
                        <table class="table pruebatabla">
                            <thead class="thead-dark"><tr>
                                <th>Operador</th>
                                <th> Nombre</th>
                                <th> Linea</th>
                                <th> Tipo</th>
                                </tr></thead>
            <%for(Renfe e : RenfeController.getAll()){%>
                <tr>
                    <td>><%=e.operator%></td>
                    <td><%=e.name%></td>
                    <td><%=e.line%></td>
                    <td><%=e.type%></td>
                </tr>
                <%}%>
                        </table>
                    </div> 

 </div>
    <div class="row">
            <div class="col 4">
                    <table class="table pruebatabla">
                        <thead class="thead-dark"><tr>
                            <th>Nombre</th>
                            <th> Longitud</th>
                            <th> Latitud</th>
                            <th> Imagen</th>
                            
                            </tr></thead>
        <%for(TRAM_stops e :TramController.get_stops_info()){%>
            <tr>
                <td>><%=e.name%></td>
                <td><%=e.longitude%></td>
                <td><%=e.latitude%></td>
                <td><%=e.image%></td>
            
            </tr>
            <%}%>
                    </table>
                </div>

                <div class="row">
                        <div class="col 4">
                                <table class="table pruebatabla">
                                    <thead class="thead-dark"><tr>
                                        <th>Nombre</th>
                                        <th> code</th>
                                        <th> id</th>
                                        <th> Imagen</th>
                                        
                                        </tr></thead>
                    <%for(TRAM_lines e :TramController.get_lines_info()){%>
                        <tr>
                            <td>><%=e.name%></td>
                            <td><%=e.code%></td>
                            <td><%=e.id%></td>
                            <td><%=e.image%></td>
                        
                        </tr>
                        <%}%>
                                </table>
                            </div>
            
    </div>
</div>

</body>
</html>