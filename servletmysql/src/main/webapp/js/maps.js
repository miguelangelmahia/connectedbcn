// OBTENCION DEL GPS Y SUS COORDENADAS
var lon = null;
var lat = null;
var map;
var cadena = "";


function gps() {
    console.log("inicio gps...");

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (objPosition) {
            lon = objPosition.coords.longitude;
            lat = objPosition.coords.latitude;
            cadena = "Latitud: " + lat + " Longitud: " + lon;
            console.log("Latitud: " + lat + " Longitud: " + lon);
            $("#geolocation").text(cadena);
            if (lat != null && lon != null) {
                if (typeof Microsoft !== undefined && typeof Microsoft.Maps !== undefined &&
                    Microsoft.Maps.Map !== null) {

                    map = new Microsoft.Maps.Map(document.getElementById('myMap'), {});
                }
                mapa();
            }
        });
    } else {
        console.log("Su navegador no soporta la API de geolocalización.");
    }
}
function mapa() {

    if (lat != null && lon != null) {
        var coord = new Microsoft.Maps.Location(lat, lon);
        var pushpin = new Microsoft.Maps.Pushpin(coord, {
            icon: 'https://www.bingmapsportal.com/Content/images/poi_custom.png',
            anchor: new Microsoft.Maps.Point(12, 39)
        });
        map.entities.push(pushpin);
    }

}

function posiciones_cercanas(lat, lon) {
    console.log("prueba de datos envio..");
    var url = "http://localhost:8080/servletmysql/api/transporte";
    var datos = { app_key: "cnbcn", api_key: "123456", option: "cercania", latitud: lat, longitud: lon };
    $.getJSON(url, datos, function (datos_recividos) {
        console.log("d: " + datos_recividos);
        console.log("d_r: " + datos_recividos.renfe);
        $.each(datos_recividos.renfe, function (val) {
            console.log("valor: " + val);
        });
    });
}


function estaciones_cercanas(latitud,longitud) {
    var url = "http://localhost:8080/servletmysql/api/transporte";
    var datos = { app_key: "cnbcn", api_key: "123456", option: "cercania", latitud: latitud, longitud: longitud };

    $.getJSON(url, datos,
        function (data) {
           console.log("estamos en jsonget");
            var stations = data.bicing;
            mapa();
            if ($("#gridCheck1").is(":checked")) {
                var stations = data.bicing;
                pintaEstacionesbicing(stations);
            } 
            if ($("#gridCheck3").is(":checked")) {
                var stations = data.renfe;
                pintaEstacionesrenfe(stations);
            } if ($("#gridCheck2").is(":checked")) {
                var stations = data.tram;
                pintaEstacionestram(stations);
            }
            if ($("#gridCheck2").is(":checked")) {
                var stations = data.metro;
                pintaEstacionesmetro(stations);
            }
            if ($("#gridCheck0").is(":checked")) {
                var stations = data.bicibox;
                pintaEstacionesbicibox(stations);
            }
        });
};

$(document).on("click", "#enviar", function () {
    map.entities.clear();
    mapa();
    estaciones_cercanas(lat,lon);
    peticion_destino();

})

function pintaEstacionesbicing(stations) {
   
    for (var i = 0; i < stations.length; i++) {
        var estacionactual = stations[i];
        var coord = new Microsoft.Maps.Location(estacionactual.latitude, estacionactual.longitude);
        var pushpin = new Microsoft.Maps.Pushpin(coord, { color: 'red', text: 'B', title: estacionactual.name });
        map.entities.push(pushpin);

    }
}

function pintaEstacionesrenfe(stations) {  
    for (var i = 0; i < stations.length; i++) {
        var estacionactual = stations[i];
        var coord = new Microsoft.Maps.Location(estacionactual.lat, estacionactual.lon);
        var pushpin = new Microsoft.Maps.Pushpin(coord, { text: 'R', title: estacionactual.name });
        map.entities.push(pushpin);
    }
}

function pintaEstacionestram(stations) {

    for (var i = 0; i < stations.length; i++) {
        var estacionactual = stations[i];
        var coord = new Microsoft.Maps.Location(estacionactual.latitude, estacionactual.longitude);
        var pushpin = new Microsoft.Maps.Pushpin(coord, { color: 'green', text: 'T', title: estacionactual.name });
        map.entities.push(pushpin);
    }
}

function pintaEstacionesmetro(stations) {
    for (var i = 0; i < stations.length; i++) {
        var estacionactual = stations[i];
        var coord = new Microsoft.Maps.Location(estacionactual.ubicacion.Latitud, estacionactual.ubicacion.Longitud);
        var pushpin = new Microsoft.Maps.Pushpin(coord, { color: 'blue', text: 'M', title: estacionactual._nom_estacio });
        map.entities.push(pushpin);
    }
}

function pintaEstacionesbicibox(stations) {
    for (var i = 0; i < stations.length; i++) {
        var estacionactual = stations[i];
        var coord = new Microsoft.Maps.Location(estacionactual.lat, estacionactual.longitud);
        var pushpin = new Microsoft.Maps.Pushpin(coord, { text: 'BiciBox', title: estacionactual.buits });
        map.entities.push(pushpin);
    }
}

function peticion_destino() {
    destinofinal = $("#direccion").val();
    console.log(destinofinal);
    var street = encodeURI(destinofinal);
    var url = "http://localhost:8080/servletmysql/api/transporte";
    var datos = { app_key: "cnbcn", api_key: "123456", option: "datos_dir", direccion: street};
    $.getJSON(url ,datos, function (data) {
            var coord = new Microsoft.Maps.Location(data.coordenadas.Latitud, data.coordenadas.Longitud);
            var pushpin = new Microsoft.Maps.Pushpin(coord, {icon:'img/poi_custom.png',  title: 'Destino' });
            map.entities.push(pushpin);
            estaciones_cercanas(data.coordenadas.Latitud,data.coordenadas.Longitud);


        });
}
$(document).on("keypress", function(e){
    if(e.which == 13){
        map.entities.clear();
        mapa();
   estaciones_cercanas(lat,lon);
   peticion_destino();
  
    }
})