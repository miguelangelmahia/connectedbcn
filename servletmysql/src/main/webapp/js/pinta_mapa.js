var Longitud_inicio = null;
var Latitud_inicio = null;
var map;
var Longitud_destino = null;
var Latitud_destino = null;

var street = "";
var Bicing = false;
var Met_tram = false;
var renfe = false;
var caminando = false;


$("#direccion").change(function (data) {
    street = $(this).val();
    // console.log($(this).val());
});
$("#gridCheck1").change(function (data) {
    Bicing = $(this).prop('checked');
    // console.log("Bici: " + $(this).prop('checked'));
});
$("#gridCheck2").change(function (data) {
    Met_tram = $(this).prop('checked');
    // console.log("Metro/tram: " + $(this).prop('checked'));
});
$("#gridCheck3").change(function (data) {
    renfe = $(this).prop('checked');
    // console.log("renfe: " + $(this).prop('checked'));
});
$("#gridCheck4").change(function (data) {
    caminando = $(this).prop('checked');
    // console.log("caminando: " + $(this).prop('checked'));
});


function gps() {
    // console.log("inicio gps...");

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (objPosition) {
            Longitud_inicio = objPosition.coords.longitude;
            Latitud_inicio = objPosition.coords.latitude;
            cadena = "Latitud: " + Latitud_inicio + " Longitud: " + Longitud_inicio;
            // console.log("Latitud: " + Latitud_inicio + "\nLongitud: " + Longitud_inicio);

            $("#geolocation").text(cadena);
            if (Latitud_inicio != null && Longitud_inicio != null) {
                if (typeof Microsoft !== undefined && typeof Microsoft.Maps !== undefined &&
                    Microsoft.Maps.Map !== null) {
                    //Map API available add your map load code.
                    map = new Microsoft.Maps.Map(document.getElementById('myMap'), {});
                }
                pushpin_Image(Latitud_inicio, Longitud_inicio, "red", 'https://www.bingmapsportal.com/Content/images/poi_custom.png', "", "");
                // estaciones_cercanas(Latitud_inicio, Longitud_inicio, "origen");
            }
        });
    } else {
        console.log("Su navegador no soporta la API de geolocalización.");
    }
}



// 'img/poi_custom.png'
// 'https://www.bingmapsportal.com/Content/images/poi_custom.png'

function pushpin_Image(latitud, longitud, color, icono, nombre, titulo) {
    // console.log("pin");
    if (latitud != null && longitud != null) {

        var coord = new Microsoft.Maps.Location(latitud, longitud);
        var pushpin = new Microsoft.Maps.Pushpin(coord, {
            icon: icono,
            color: color,
            title: titulo,
            text: nombre,
            anchor: new Microsoft.Maps.Point(12, 39)
        });
        map.entities.push(pushpin);
    }

}


$(document).on("click", "#enviar", function () {
    map.entities.clear();
    pushpin_Image(Latitud_inicio, Longitud_inicio, "red", 'https://www.bingmapsportal.com/Content/images/poi_custom.png', "", "");
    if(Longitud_destino != null && Latitud_destino != null){
        pushpin_Image(Latitud_destino, Longitud_destino, "yellow", 'img/poi_custom.png', "", "");
    }


    console.log("Calle: " +street + "\nBicing: " + Bicing + "\nMet_tram: " + Met_tram + "\nRenfe: " + renfe + "\nCaminando: " + caminando);
    
    if(Bicing){
        estaciones_cercanas(Latitud_inicio,Longitud_inicio,"origen");
        if(street != "" ){
           coordenadas_de_direccion_destino(street);        
        }
    }else if(Met_tram){
        estaciones_cercanas(Latitud_inicio,Longitud_inicio,"origen");
        if(street != "" ){
           coordenadas_de_direccion_destino(street);        
        }
    }else if(renfe){
        estaciones_cercanas(Latitud_inicio,Longitud_inicio,"origen");
        if(street != "" ){
           coordenadas_de_direccion_destino(street);        
        }
    }else if(caminando){

    }else{
        if(street != "" ){
            coordenadas_de_direccion_destino(street);
        }
        console.log("No hay transportes seleccionados...")
    }

})

function coordenadas_de_direccion_destino(direccion) {

    var street = encodeURI(direccion);
    // console.log("dest: " + street)
    var url = "http://localhost:8080/servletmysql/api/transporte";
    var datos = { app_key: "cnbcn", api_key: "123456", option: "datos_dir", direccion: street};
    $.getJSON(url ,datos, function (data) {
          
            console.log("Coord_direccion: " +data.nombre+"\nLatitud destino: "+ data.coordenadas.Latitud+"\nLongitud destino: " + data.coordenadas.Longitud);
            
            Latitud_destino = data.coordenadas.Latitud;
            Longitud_destino = data.coordenadas.Longitud;
            pushpin_Image(Latitud_destino, Longitud_destino, "yellow", 'img/poi_custom.png', "", "");
            estaciones_cercanas(Latitud_destino, Longitud_destino,"destino");
    });
}


// http://localhost:8080/servletmysql/api/transporte?app_key=cnbcn&api_key=123456&option=datos_dir&direccion=pintor%20fortuny%2067,%20barbera%20del%20valles,%2008210


function estaciones_cercanas(latitud, longitud,position) {
    var url = "http://localhost:8080/servletmysql/api/transporte";
    var datos = { app_key: "cnbcn", api_key: "123456", option: "cercania", latitud: latitud, longitud: longitud };
    console.log("Estaciones cercanas.\nLatitud inicio" + latitud + "\nLongitud inicio" + longitud + "\n" + position);

    $.getJSON(url, datos,
        function (data) {
          

            if(position == "origen"){
                
                for (var i = 0; i < data.bicing.length; i++) {
                    var estacionactual = data.bicing[i];
                    pushpin_Image(estacionactual.latitude, estacionactual.longitude, "red", null, "B", estacionactual.name)
                }

                for (var i = 0; i < data.renfe.length; i++) {
                    var estacionactual = data.renfe[i];
                    pushpin_Image(estacionactual.lat, estacionactual.lon, null, null, "R", "")
                }

                for (var i = 0; i < data.tram.length; i++) {
                    var estacionactual = data.tram[i];
                    pushpin_Image(estacionactual.latitude, estacionactual.longitude, "green", null, "T", estacionactual.name)
                }

                for (var i = 0; i < data.metro.length; i++) {
                    var estacionactual = data.metro[i];
                    pushpin_Image(estacionactual.lat, estacionactual.lon, "blue", null, "M", estacionactual._nom_estacio)
                }

                
            }else if (position == "destino"){
                
                for (var i = 0; i < data.renfe.length; i++) {
                    var estacionactual = data.renfe[i];
                    pushpin_Image(estacionactual.lat, estacionactual.lon, null, null, "R", "")
                }
            
            
            }

        });
};