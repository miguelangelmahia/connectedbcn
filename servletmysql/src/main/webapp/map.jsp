<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Plantilla</title>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
		integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
		integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	
	<link rel="stylesheet" href="css/estilosPlantilla.css">

	<style>
		#myMap {
			margin-top: 3%;
			margin-bottom: 3%;
			border-radius: 5px;
			position:relative;
		}

		.formulario {
			width: 50%;
			min-width: 500px;
		}

		.forma {
			border-radius: 5px;
			box-shadow: 1px 1px 10px 1px rgba(0, 0, 0, 0.75);
			margin: 15px 0;
			padding: 15px;
			color: #008b3b;
		}
		.bt{
			left: 17%;
    		position: relative;
		}
		div#contenedor_position{
	    	background-color: #67af749c;
    		display: inline-block;
    		position: absolute;
   			padding: 2%;
    		opacity: 0.5;
    		opacity: 0.788;
    		right: 74%;
    		top: 9%;
    		border-radius: 10px;
    		z-index: 1;
    		left: 3%;
		}
	</style>
</head>

<body >
	<nav class="navbar navbar-expand-lg menu">
		<a class="navbar-brand" href="#">ConnectedBCN</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<i class="fas fa-bars menu-comprimido"></i>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link link-menu" href="#">Inicio <span
							class="sr-only">(current)</span>
					</a></li>
				<li class="nav-item"><a class="nav-link link-menu" href="#">Bicing</a></li>
				<li class="nav-item"><a class="nav-link link-menu" href="#">Metro</a></li>
				<li class="nav-item"><a class="nav-link link-menu" href="#">TRAM</a></li>
				<li class="nav-item"><a class="nav-link link-menu" href="#">eCooltra</a>
				</li>
			</ul>
			<button class="boton-menu" type="submit">
				Entra <i class="fas fa-sign-in-alt"></i>
			</button>
			<button class="boton-menu" type="submit">
				Regístrate <i class="fas fa-user-plus"></i>
			</button>
		</div>
	</nav>
	<div class="container">
		<div class="row">
			<div class="col 12">
				
				<div id="contenedor_position">
				<i class="fas fa-location-arrow"> Localizacion</i>
				<a id="geolocation"></a>
				</div>
				<div id='myMap'  style='width: 100%; height: 500PX;'></div>
			</div>
		</div>
		<div class="row d-flex justify-content-center">
			<div class="col-12 col-sm-9 col-md-6">



				<div class="forma">
					<div class="form-group">
						<label for="direccion"><i class="fas fa-route"></i> Direccion
						</label> <input type="text" class="form-control" id="direccion" placeholder="nombre de calle, numero de la calle, cuidad...">
					</div>

					
					<div class="form-group row">
						<div class="col-sm-10">
							<div class="form-check">
								<input class="form-check-input" type="checkbox" id="gridCheck0">
								<label class="form-check-label" for="gridCheck0">Bicicleta propia</label>
							</div>
							<div class="form-check" id="cargarbicing">
								<input class="form-check-input" type="checkbox" id="gridCheck1">
								<label class="form-check-label" for="gridCheck1">Bicing</label>
							</div>
							<div class="form-check">
								<input class="form-check-input" type="checkbox" id="gridCheck2">
								<label class="form-check-label" for="gridCheck2">Metro/Tram</label>
							</div>
							<div class="form-check">
								<input class="form-check-input" type="checkbox" id="gridCheck3">
								<label class="form-check-label" for="gridCheck3">Renfe</label>
							</div>
							<%-- <div class="form-check">
								<input class="form-check-input" type="checkbox" id="gridCheck4">
								<label class="form-check-label" for="gridCheck4">Caminando</label>
							</div> --%>
							<div class="text-right bt">
							<button id="enviar" type="button" class="btn btn-outline-success">Buscar</button>
							</div>

						</div>
					</div>
					
				</div>


			</div>
		</div>
	</div>
	<footer class="container-fluid">
		<div class="row">
			<div class="col-12 redes-sociales">
				<div class="row d-flex justify-content-around">
					<div class="col-3 col-sm-2 d-flex justify-content-center">
						<div class="facebook d-flex justify-content-around align-items-center">
							<i class="fab fa-facebook-f"></i>
						</div>
					</div>
					<div class="col-3 col-sm-2 d-flex justify-content-center">
						<div class="google-plus d-flex justify-content-around align-items-center">
							<i class="fab fa-google-plus-g"></i>
						</div>
					</div>
					<div class="col-3 col-sm-2 d-flex justify-content-center">
						<div class="instagram d-flex justify-content-around align-items-center">
							<i class="fab fa-instagram"></i>
						</div>
					</div>
					<div class="col-3 col-sm-2 d-flex justify-content-center">
						<div class="twitter d-flex justify-content-around align-items-center">
							<i class="fab fa-twitter"></i>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12">
				<div class="row">
					<div class="col-12 col-md-6 text-center contacto text-center">
						<p>
							Ponte en contacto con nosotros pulsando <a href="#">aquí</a>.
						</p>
					</div>
					<div class="col-12 col-md-6 text-center copyright text-center">
						<p>@Copyright 2019 ConnectedBCN</p>
					</div>
				</div>
			</div>
		</div>
	</footer>
<script type='text/javascript'
		src='https://www.bing.com/api/maps/mapcontrol?key=Auf4Ic9Y4ZgEO2feZ4X0Gizx1goLhhrNNFKu4tEgmp9Q2GV-N8aXQkOieKftfxyX&callback=gps' async defer></script>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
		<script type="text/javascript" src="js/maps.js"></script>
	<script>
		
	</script>


</body>

</html>