package com.codesplai.peticiones;

public class Estaciones {

    public Coordinates ubicacion;
    public int _codi_estacio;
    public int _codi_estacio_linea;
    public int _codi_grup_estacio;
    public int _codi_linea;
    public String _color_linea;
    public String _data;
    public String _data_inaguracio;
    public String _desc_servei;
    public String _desti_servei;
    public int _id_estacio;
    public int _id_estacio_linea;
    public int _id_linea;
    public int _id_tipus_accesibilitat;
    public int _id_tipues_estat;
    public int _id_tipus_servei;
    public String _nom_estacio;
    public String _nom_linea;
    public String _nom_tipus_accesibilitat;
    public String _nom_tipus_estat;
    public int _ordre_estacio;
    public int _ordre_linea;
    public String _origen_servei;
    public String _picto;
    public String _picto_grup;
    @Override
    public String toString() {
        return _nom_estacio;
    }
}