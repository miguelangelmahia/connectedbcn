package com.codesplai.peticiones;

import com.google.gson.Gson;

public class Mapa {
    public String nombre;
    Coordinates coordenadas;
    // constructor vacio
    public Mapa() {
    }

    // constructor con todo
    public Mapa(String nombre, Coordinates coordenadas) {
        this.nombre = nombre;
        
    }

    @Override
    public String toString() {
        return String.format("nombre:%s  coordenadas:%d image:%s id:%d ", this.nombre, this.coordenadas);
    }
    public String to_json() {
        String coordenadas = new Gson().toJson(this.coordenadas);

        // return "{\"nombre\": "+ nombre+", \"coordenadas\" : "+coordenadas +" }";
        return "{ \"nombre\": " + nombre + ", \"coordenadas\": " + coordenadas + " }";



    }
}