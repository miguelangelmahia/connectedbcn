package com.codesplai.peticiones;
import com.codesplai.basedatos.*;
import java.util.*;


public class View {
    public static String[] Header_all_Boostrap(String titulo_pagina) {
        // <head>
        // <meta charset="UTF-8">
        // <meta name="viewport" content="width=device-width, initial-scale=1.0">
        // <meta http-equiv="X-UA-Compatible" content="ie=edge">
        // <link rel="stylesheet"
        // href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        // integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
        // crossorigin="anonymous">
        // <title>Peticiones</title>
        // </head>
        String[] cadena = { "<head>", "<meta charset=\"UTF-8\">",
                "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">",
                "<meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">", "<link rel=\"stylesheet\"",
                "href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\"",
                "integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\"",
                "crossorigin=\"anonymous\">", "<title>", titulo_pagina, "</title>", "</head>" };

        return cadena;
    }

    public static List<String> Convert_Usuarios(Usuarios us){
        List<String> lista = new ArrayList<String>();
        lista.add(Integer.toString( us.id));
        lista.add((String)us.email);
        lista.add((String)us.user);
        lista.add((String)us.password);
        lista.add((String)us.telefono);
        // Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        // String d = formatter.format(us.nacimiento);
        // lista.add(d);
        lista.add((String)us.urlFoto);


        return lista;
    }

    public static List<String> Lista_boostrap(String[] titulos, List<String> parametros) {
     
         List<String> lista = new ArrayList<String>();
        lista.add("<table class=\"table table-dark\">");
        lista.add("<thead>");
        lista.add("<tr>");
        for(String n : titulos){
            // <th>n</th>
            String cadena = "<th>" + n + "</th>";
            lista.add(cadena);
        }
        lista.add("</tr>");
        lista.add("</thead>");

        lista.add("<tbody>");
        for(String n : parametros){
           
           
            lista.add(n);
        }

        lista.add("</tbody>");



        lista.add("</table>");
        return lista;
    }

    public static String[] Bootstrap_Scrips() {
        // <script src=\"https://code.jquery.com/jquery-3.3.1.min.js\"></script>
        // <script
        // src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js\"
        // integrity=\"sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1\"
        // crossorigin=\"anonymous\"></script>
        // <script
        // src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js\"
        // integrity=\"sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM\"
        // crossorigin=\"anonymous\"></script>
        String[] text = { " <script src=\"https://code.jquery.com/jquery-3.3.1.min.js\"></script>",
                "<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js\" integrity=\"sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1\" crossorigin=\"anonymous\"></script>",
                "<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js\" integrity=\"sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM\" crossorigin=\"anonymous\"></script>" };
        return text;
    }

    public static String Titulos_h1(String texto) {
        return "<h1>" + texto + "</h1>";
    }

    public static String Titulos_h2(String texto) {
        return "<h2>" + texto + "</h2>";
    }

    public static String Titulos_h3(String texto) {
        return "<h3>" + texto + "</h3>";
    }

    public static String Titulos_h4(String texto) {
        return "<h4>" + texto + "</h4>";
    }

    public static String Titulos_h5(String texto) {
        return "<h5>" + texto + "</h5>";
    }

    public static String Titulos_h6(String texto) {
        return "<h6>" + texto + "</h6>";
    }

    public static String Titulos_p(String texto) {
        return "<p>" + texto + "</p>";
    }
}