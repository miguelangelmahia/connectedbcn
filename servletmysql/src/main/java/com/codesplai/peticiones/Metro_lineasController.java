package com.codesplai.peticiones;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.Gson;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class Metro_lineasController {

    public static String getAll_cad() throws IOException {

        String urls = "https://api.tmb.cat/v1/transit/linies/metro?app_id=fe45ede7&app_key=71e88ac0e394d61b622c34d29a624b72";
       
        URL url = new URL(urls);
        URLConnection conn = url.openConnection();
        InputStream is = conn.getInputStream();
        BufferedInputStream bis = new BufferedInputStream(is);
        String strFileContents = "";
        try {

            byte[] contents = new byte[1024];

            int bytesRead = 0;

            while ((bytesRead = bis.read(contents)) != -1) {
                strFileContents += new String(contents, 0, bytesRead);
            }

            

        } catch (Exception e) {
            System.out.println(e.toString());
        }

       
        return strFileContents;
    }

 
    public static List<Metro_lineas> getAll() throws IOException {

        List<Metro_lineas> mt = new ArrayList<Metro_lineas>();

        System.setProperty("http.proxyHost", "...");
        System.setProperty("http.proxyPort", "8080");

        String urls = "https://api.tmb.cat/v1/transit/linies/metro?app_id=fe45ede7&app_key=71e88ac0e394d61b622c34d29a624b72";
        // System.out.println(urls);
        URL url = new URL(urls);
        URLConnection conn = url.openConnection();
        InputStream is = conn.getInputStream();
        BufferedInputStream bis = new BufferedInputStream(is);
        String strFileContents = "";
        try {

            byte[] contents = new byte[1024];

            int bytesRead = 0;

            while ((bytesRead = bis.read(contents)) != -1) {
                strFileContents += new String(contents, 0, bytesRead);
            }

            Gson gson = new Gson();
            JsonElement element = gson.fromJson(strFileContents, JsonElement.class);
            JsonObject jsonObj = element.getAsJsonObject();

            JsonArray arrayElementos = jsonObj.getAsJsonArray("features");

            for (JsonElement e : arrayElementos) {
                Metro_lineas metro = new Metro_lineas();
                JsonObject n = e.getAsJsonObject();
                JsonObject proper = n.getAsJsonObject("properties");


                // System.out.println("prueba...");
                metro._codi_familia = Integer.parseInt((proper.get("CODI_FAMILIA")!=null) ? proper.get("CODI_FAMILIA").toString():"0");
                metro._codi_linia = Integer.parseInt((proper.get("CODI_LINIA")!=null) ? proper.get("CODI_LINIA").toString():"0");
                metro._codi_tipus_calendari = (proper.get("CODI_TIPUS_CALENDARI")!=null) ? proper.get("CODI_TIPUS_CALENDARI").toString():"";
                metro._color_aux_linia = (proper.get("COLOR_AUX_LINIA")!=null) ? proper.get("COLOR_AUX_LINIA").toString():"";
                metro._color_linia = (proper.get("COLOR_LINIA")!=null) ? proper.get("COLOR_LINIA").toString():"";
                metro._data = (proper.get("DATA")!=null) ? proper.get("DATA").toString():"";
                metro._desc_linia = (proper.get("DESC_LINIA")!=null) ? proper.get("DESC_LINIA").toString():"";
                metro._desti_linia = (proper.get("DESTI_LINIA")!=null) ? proper.get("DESTI_LINIA").toString():"";
                metro._id_linia = Integer.parseInt((proper.get("ID_LINIA")!=null) ? proper.get("ID_LINIA").toString():"0");
                metro._id_operador = Integer.parseInt((proper.get("ID_OPERADOR")!=null) ? proper.get("ID_OPERADOR").toString():"0");
                metro._nom_familia = (proper.get("NOM_FAMILIA")!=null) ? proper.get("NOM_FAMILIA").toString():"";
                metro._nom_linia = (proper.get("NOM_LINIA")!=null) ? proper.get("NOM_LINIA").toString():"";
                metro._nom_operador = (proper.get("NOM_OPERADOR")!=null) ? proper.get("NOM_OPERADOR").toString():"";
                metro._nom_tipus_calendari = (proper.get("NOM_TIPUS_CALENDARI")!=null) ? proper.get("NOM_TIPUS_CALENDARI").toString():"";
                metro._nom_tipus_transport = (proper.get("NOM_TIPUS_TRANSPORT")!=null) ? proper.get("NOM_TIPUS_TRANSPORT").toString():"";
                metro._num_paquets = Integer.parseInt((proper.get("NUM_PAQUETS")!=null) ? proper.get("NUM_PAQUETS").toString():"0");
                metro._ordre_familia = Integer.parseInt((proper.get("ORDRE_FAMILIA")!=null) ? proper.get("ORDRE_FAMILIA").toString():"0");
                metro._ordre_linia = Integer.parseInt((proper.get("ORDRE_LINIA")!=null) ? proper.get("ORDRE_LINIA").toString():"0");
                metro._origin_linea = (proper.get("ORIGEN_LINIA")!=null) ? proper.get("ORIGEN_LINIA").toString():"";


                // System.out.println(proper.get("NOM_LINIA").toString());

                JsonElement properties = n.get("geometry");
                JsonObject Obj = properties.getAsJsonObject();
                JsonArray arrayE = Obj.getAsJsonArray("coordinates");
                if (arrayE!=null){

                    for (JsonElement o : arrayE) {
                        
                        JsonArray array1 = o.getAsJsonArray();
                        if (array1!=null){

    
                            for (JsonElement i : array1) {
                                String lon = (i.getAsJsonArray().get(0)!=null) ? i.getAsJsonArray().get(0).toString() : "0";
                                String lat = (i.getAsJsonArray().get(1)!=null) ? i.getAsJsonArray().get(1).toString() : "0";
                                Coordinates coor = new Coordinates(lon, lat);
                                // System.out.println("coords: "+lon+","+lat);
                                metro.set_dibujo_linea(coor);
                            }
                        }
                    }
                }

                metro.set_Lista_estation(getAll_estaciones(Integer.parseInt(proper.get("CODI_LINIA").toString())));
                mt.add(metro);
              
            }

        } catch (Exception e) {
            System.out.println(e.toString());
        }

        // for(Metro_lineas metro : mt){
            
        //     for(Estaciones er : metro.estations){
        //         System.out.println(metro._nom_linia + " || " + er.toString());
        //     }
         
        // }

        return mt;
    }

    public static List<Estaciones> getAll_estaciones(int codi_linea) throws IOException {
        List<Estaciones> Est = new ArrayList<Estaciones>();
        System.setProperty("http.proxyHost", "...");
        System.setProperty("http.proxyPort", "8080");
        String urls = "https://api.tmb.cat/v1/transit/linies/metro/" + codi_linea
                + "/estacions?app_id=fe45ede7&app_key=71e88ac0e394d61b622c34d29a624b72";
        // System.out.println(urls);
        URL url = new URL(urls);
        URLConnection conn = url.openConnection();
        InputStream is = conn.getInputStream();
        BufferedInputStream bis = new BufferedInputStream(is);
        String strFileContents = "";
        try {

            byte[] contents = new byte[1024];

            int bytesRead = 0;

            while ((bytesRead = bis.read(contents)) != -1) {
                strFileContents += new String(contents, 0, bytesRead);
            }

            Gson gson = new Gson();
            JsonElement element = gson.fromJson(strFileContents, JsonElement.class);
            JsonObject jsonObj = element.getAsJsonObject();

            JsonArray arrayElementos = jsonObj.getAsJsonArray("features");

            for (JsonElement estation : arrayElementos) {
                Estaciones es = new Estaciones();
                JsonObject proper = estation.getAsJsonObject();
                JsonObject properties = proper.getAsJsonObject("properties");
                JsonObject geometries = proper.getAsJsonObject("geometry");

                es._codi_estacio = Integer.parseInt((properties.get("CODI_ESTACIO") != null) ? properties.get("CODI_ESTACIO").toString() : "");
                es._codi_estacio_linea = Integer.parseInt((properties.get("CODI_ESTACIO_LINIA") != null) ? properties.get("CODI_ESTACIO_LINIA").toString() : "");
                es._codi_grup_estacio = Integer.parseInt((properties.get("CODI_GRUP_ESTACIO") != null) ? properties.get("CODI_GRUP_ESTACIO").toString() : "");
                es._codi_linea = Integer.parseInt((properties.get("CODI_LINIA") != null) ? properties.get("CODI_LINIA").toString() : "");

                es._color_linea = (properties.get("COLOR_LINIA") != null) ? properties.get("COLOR_LINIA").toString() : "";  
                es._data = (properties.get("DATA") != null) ? properties.get("DATA").toString() : "";  
                es._data_inaguracio = (properties.get("DATA_INAUGURACIO") != null) ? properties.get("DATA_INAUGURACIO").toString() : "";  
                es._desc_servei = (properties.get("DESC_SERVEI") != null) ? properties.get("DESC_SERVEI").toString() : "";  
                es._desti_servei = (properties.get("DESTI_SERVEI") != null) ? properties.get("DESTI_SERVEI").toString() : "";  

                es._id_estacio = Integer.parseInt((properties.get("ID_ESTACIO") != null) ? properties.get("ID_ESTACIO").toString() : "");
                es._id_estacio_linea = Integer.parseInt((properties.get("ID_ESTACIO_LINIA") != null) ? properties.get("ID_ESTACIO_LINIA").toString() : "");
                es._id_linea = Integer.parseInt((properties.get("ID_LINIA") != null) ? properties.get("ID_LINIA").toString() : "");
                es._id_tipus_accesibilitat = Integer.parseInt((properties.get("ID_TIPUS_ACCESSIBILITAT") != null) ? properties.get("ID_TIPUS_ACCESSIBILITAT").toString() : "");
                es._id_tipues_estat = Integer.parseInt((properties.get("ID_TIPUS_ESTAT") != null) ? properties.get("ID_TIPUS_ESTAT").toString() : "");
                es._id_tipus_servei = Integer.parseInt((properties.get("ID_TIPUS_SERVEI") != null) ? properties.get("ID_TIPUS_SERVEI").toString() : "");

                es._nom_estacio = (properties.get("NOM_ESTACIO") != null) ? properties.get("NOM_ESTACIO").toString() : "";  
                es._nom_estacio=es._nom_estacio.split("\"")[1];
                es._nom_linea = (properties.get("NOM_LINIA") != null) ? properties.get("NOM_LINIA").toString() : "";  
                // es._nom_linea=es._nom_linea.split("\"")[1];
                es._nom_tipus_accesibilitat = (properties.get("NOM_TIPUS_ACCESSIBILITAT") != null) ? properties.get("NOM_TIPUS_ACCESSIBILITAT").toString() : "";  
                es._nom_tipus_estat = (properties.get("NOM_TIPUS_ESTAT") != null) ? properties.get("NOM_TIPUS_ESTAT").toString() : "";  

                es._ordre_estacio = Integer.parseInt((properties.get("ORDRE_ESTACIO") != null) ? properties.get("ORDRE_ESTACIO").toString() : "");
                es._ordre_linea = Integer.parseInt((properties.get("ORDRE_LINIA") != null) ? properties.get("ORDRE_LINIA").toString() : "");

                es._origen_servei = (properties.get("ORIGEN_SERVEI") != null) ? properties.get("ORIGEN_SERVEI").toString() : "";  
                es._picto = (properties.get("PICTO") != null) ? properties.get("PICTO").toString() : "";  
                es._picto_grup = (properties.get("PICTO_GRUP") != null) ? properties.get("PICTO_GRUP").toString() : "";  

                // System.out.println(properties.get("NOM_ESTACIO").toString() + " || " + properties.get("NOM_LINIA").toString());
                JsonArray arrayElementos1 = geometries.getAsJsonArray("coordinates");
                Coordinates coord = new Coordinates(arrayElementos1.get(0).toString(),
                        arrayElementos1.get(1).toString());
                es.ubicacion = coord;
                Est.add(es);
            }

        } catch (Exception e) {
            System.out.println(e.toString());


        }

        return Est;
    }

}