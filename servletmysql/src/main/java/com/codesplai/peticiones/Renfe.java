package com.codesplai.peticiones;

public class Renfe {
    public String id;
    public String lat;
    public String line;
    public String lon;
    public String name;
    public String operator;
    public String type;
    public String zone;

    // constructor vacio
    public Renfe() {

    }

    // constructor completo
    public Renfe(String id, String lat, String line, String lon, String name, String operator,  String type, String zone) {
        this.id = id;
        this.lat = lat;
        this.line = line;
        this.lon = lon;
        this.name = name;
        this.operator = operator;
        this.type = type;
        this.zone = zone;

    }

    @Override
    public String toString() {
        return String.format("id:%s  latitud:%s line:%s lon:%s name:%s operator:%s  type:%s Zona:%s ", this.id, this.lat,
                this.line,  this.lon, this.name, this.operator, this.type, this.zone);
    }

}