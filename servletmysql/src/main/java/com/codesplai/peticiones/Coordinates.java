package com.codesplai.peticiones;


public class Coordinates{

   public String Longitud;
   public String Latitud;

    public Coordinates(String lon, String lat){
        this.Longitud = lon;
        this.Latitud = lat;
    }
    @Override
    public String toString() {
        return String.format("Lat:%s , Lon:%s",this.Latitud,this.Longitud);
    }
}