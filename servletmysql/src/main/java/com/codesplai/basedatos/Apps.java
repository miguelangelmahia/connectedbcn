package com.codesplai.basedatos;


public class Apps {
    public int id_usuario;
    public int id;
    public String nombre;
    public String apikey;
  
    public Apps(){

   }
    public Apps(int id_usuario, int id, String nombre, String apikey) {
        
        this.id_usuario = id_usuario;
        this.id =id;
        this.nombre = nombre;
        this.apikey = apikey;   
    }

    @Override
    public String toString() {
        return String.format("id_usuario:%d id:%d nombre:%s apikey:%s", this.id_usuario, this.id, this.nombre, this.apikey);
    }
}