package com.codesplai.basedatos;

import java.util.List;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;

import com.mysql.jdbc.Connection;

public class UsuariosController {
    private static final String TABLE = "usuarios";
    private static final String KEY1 = "id_usuario";
    // getAll devuelve todos los registros de la tabla
    public static List<Usuarios> getAll() {
        
        List<Usuarios> listaUsuarios = new ArrayList<Usuarios>();
        String sql = String.format("select * from %s;", TABLE);
        // String sql = "select id,nombre,password from "+TABLE;

        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Usuarios u = new Usuarios(rs.getInt("id_usuario"), rs.getString("email"), rs.getString("user"),
                        rs.getString("password"), rs.getString("telefono"), rs.getDate("nacimiento"),
                        rs.getString("urlfoto"));
                listaUsuarios.add(u);
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return listaUsuarios;

    }

    // AÑADIR USUARIO A UNA TABLA
    public static void create(Usuarios us) {
        // INSERT INTO `usuarios` (`id_usuario`, `email`, `user`, `password`,
        // `telefono`, `nacimiento`, `urlfoto`, `favorito`)
        // VALUES (NULL, 'jks@gmail.com', 'juanka', '123456', '999-999-999',
        // '2018-08-22', NULL, NULL);
        String sql = "INSERT INTO usuarios (id_usuario, email, user, password, telefono, nacimiento, urlfoto, developer) VALUES (NULL, ?, ?, ?, ?, NULL, NULL, NULL);";
        try (Connection conn = DBConn.getConn(); PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, us.email);
            pstmt.setString(2, us.user);
            pstmt.setString(3, us.password);
            pstmt.setString(4, us.telefono);
            pstmt.setBoolean(5, us.developer);
            pstmt.executeUpdate();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    // BORRA USUARIO
    public static void delete_Usuario(int id) {

        // DELETE FROM `Usuarios` WHERE `Usuarios`.`id_usuarios` = ?"
        String sql = String.format("DELETE FROM %s WHERE %s.%s=%d", TABLE, TABLE, KEY1, id);
        System.out.println(sql);

        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

            stmt.executeUpdate(sql);

        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
    }

    // ACTUALIZA USUARIO de(id,email,user,passwrod,telefono) con id=x
    public static void update_Usuario(Usuarios us) {
        //UPDATE usuarios SET email = 'mireia@gmail.es', password = '4331', telefono = '888-888-848' WHERE usuarios.id_usuario = 2;
        // UPDATE usuarios SET nombre = "TC_10" WHERE idUsuarios = 11;

        String sql = "UPDATE usuarios SET email = ?, user = ?, password = ?, telefono = ? WHERE usuarios.id_usuario = ?;";
        System.out.println(sql);

        try (Connection conn = DBConn.getConn(); PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, us.email);
            pstmt.setString(2, us.user);
            pstmt.setString(3, us.password);
            pstmt.setString(4, us.telefono);
            pstmt.setInt(5, us.id);
            pstmt.executeUpdate();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

   
}