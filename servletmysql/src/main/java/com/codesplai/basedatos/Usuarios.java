package com.codesplai.basedatos;

import java.util.Date;

public class Usuarios {

    public int id;
    public String email;
    public String user;
    public String password;
    public String telefono;
    public Date nacimiento;
    public String urlFoto;
    public Boolean developer;


    public Usuarios(String mail,String u, String pass, String tel) {
        
        this.email = mail;
        this.user = u;
        this.password = pass;
        this.telefono = tel;
        
        
    }
    public Usuarios(int d,String mail,String u, String pass, String tel) {
        this.id = d;
        this.email = mail;
        this.user = u;
        this.password = pass;
        this.telefono = tel;
                
    }
    public Usuarios(int d,String mail,String u, String pass, String tel, Date naci, String urFot) {
        this.id = d;
        this.email = mail;
        this.user = u;
        this.password = pass;
        this.telefono = tel;
        this.nacimiento = naci;
        this.urlFoto = urFot;
        
    }

    @Override
    public String toString() {
        return String.format("id:%d email:%s user:%s pass:%s telefono:%s nacimiento:%s url:%s", this.id, this.email, this.user, this.password, this.telefono, this.nacimiento, this.urlFoto);
    }
}