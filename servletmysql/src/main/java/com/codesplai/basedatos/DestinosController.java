package com.codesplai.basedatos;

import java.util.List;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;

import com.mysql.jdbc.Connection;

public class DestinosController {
    private static final String TABLE = "destinos";
    private static final String KEY1 = "id_destinos";
    // getAll devuelve todos los registros de la tabla
    public static List<Destinos> getAll() {
        
        List<Destinos> listaDestinos = new ArrayList<Destinos>();
        String sql = String.format("select * from %s;", TABLE);
    

        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Destinos u = new Destinos(rs.getInt("id_destinos"), rs.getString("nombre"), rs.getString("latitud"),
                        rs.getString("longitud"), rs.getInt("usuarios_id_usuario"));
                listaDestinos.add(u);
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return listaDestinos;

    }

    // AÑADIR USUARIO A UNA TABLA
    public static void create(Destinos des) {
        // INSERT INTO `usuarios` (`id_usuario`, `email`, `user`, `password`,
        // `telefono`, `nacimiento`, `urlfoto`, `favorito`)
        // VALUES (NULL, 'jks@gmail.com', 'juanka', '123456', '999-999-999',
        // '2018-08-22', NULL, NULL);
        String sql = "INSERT INTO destinos (id_destinos, nombre, latitud, longitud, usuarios_id_usuario) VALUES (NULL, ?, ?, ?, NULL);";
        try (Connection conn = DBConn.getConn(); PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, des.nombre);
            pstmt.setString(2, des.latitud);
            pstmt.setString(3, des.longitud);
            // pstmt.setDate(5, us.nacimiento);
            pstmt.executeUpdate();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    
    // BORRA USUARIO
    public static void delete_Destino(int id) {

        // DELETE FROM `Destinos` WHERE `Destinos`.`id_destino` = ?"
        String sql = String.format("DELETE FROM %s WHERE %s.%s=%d", TABLE, TABLE, KEY1, id);
        System.out.println(sql);

        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

            stmt.executeUpdate(sql);

        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
    }


    // ACTUALIZA USUARIO de(id,email,user,passwrod,telefono) con id=x
    public static void update_Destino(Destinos des) {
        //UPDATE usuarios SET email = 'mireia@gmail.es', password = '4331', telefono = '888-888-848' WHERE usuarios.id_usuario = 2;
        // UPDATE usuarios SET nombre = "TC_10" WHERE idUsuarios = 11;

        String sql = "UPDATE Destinos SET nombre = ?, latitud = ?, longitud = ? WHERE destinos.id_destinos = ?;";
        System.out.println(sql);

        try (Connection conn = DBConn.getConn(); PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, des.nombre);
            pstmt.setString(2, des.latitud);
            pstmt.setString(3, des.longitud);
            pstmt.setInt(4, des.id_destinos);
            pstmt.executeUpdate();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

   
}