package com.codesplai.basedatos;


public class Conexiones {
    public int id_apps;
    public int id;
    public String host;
    public String maquina;
    public String idsesion;
    public String llamada;
    public String type;
    public String parametros;
   
    public Conexiones(){

    }
   
   
    public Conexiones(int id_apps, int id, String host, String maquina, String idsesion, String llamada, String type, String parametros) {
        
        this.id_apps = id_apps;
        this.id =id;
        this.host = host;
        this.maquina = maquina;
        this.idsesion = idsesion;   
        this.llamada = llamada;
        this.type = type;   
        this.parametros = parametros;   

    }

    @Override
    public String toString() {
        return String.format("id_apps:%d id:%d host:%s maquina:%s idsesion:%s llamada:%s type:%s parametros:%s",
         this.id_apps, this.id, this.host, this.maquina, this.idsesion, this.llamada, this.type, this.parametros);
    }
}