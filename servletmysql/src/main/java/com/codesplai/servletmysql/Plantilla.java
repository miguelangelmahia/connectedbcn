package com.codesplai.servletmysql;

import com.codesplai.basedatos.*;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;

public class Plantilla extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		out.println("Peticion doGet...");
		String app_key = request.getParameter("app_key");
		String api_key = request.getParameter("api_key");
		if (api_key != null && app_key != null) {
			if (AppsController.comprovate_credentials(app_key, api_key)) {

			} else {
				out.println("ERROR 400");
			}
		} else {
			out.println("ERROR 399");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// create HTML response
		PrintWriter writer = response.getWriter();
		writer.println("Peticion doPost...");
		String app_key = request.getParameter("app_key");
		String api_key = request.getParameter("api_key");
		if (api_key != null && app_key != null) {
			if (AppsController.comprovate_credentials(app_key, api_key)) {

			} else {
				writer.println("ERROR 400");
			}
		} else {
			writer.println("ERROR 399");
		}
	}

}

/*
 * 
 * <servlet> 
 * <servlet-name>
 * apiRest
 * </servlet-name>
 * <servlet-class>
 * com.codesplai.servletmysql.Api
 * </servlet-class> 
 * </servlet>
 * 
 * <servlet-mapping>
 * <servlet-name>
 * apiRest
 * </servlet-name>
 * <url-pattern>
 * /api</url-pattern> 
 * </servlet-mapping>
 * 
 */