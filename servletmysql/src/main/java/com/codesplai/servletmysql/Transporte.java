package com.codesplai.servletmysql;

import com.codesplai.basedatos.*;
import com.codesplai.peticiones.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.util.List;
// import java.awt.List;
import java.io.*;
import java.util.Enumeration;
import java.util.Map;

public class Transporte extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		PrintWriter out = response.getWriter();
		System.out.println("Peticion doGet...");
		String app_key = request.getParameter("app_key");
		String api_key = request.getParameter("api_key");
		String option = request.getParameter("option");
		if (api_key != null && app_key != null) {
			if (AppsController.comprovate_credentials(app_key, api_key)) {
				escriture_data(request, "GET", app_key, api_key);

				if (option.equals("cercania")) {
					String lat = request.getParameter("latitud");
					String lon = request.getParameter("longitud");
					Obj_cercanos obj = null;
					try {
						obj = cercanus(lat, lon, 5000);
						out.print(obj.to_json());
						// System.out.println(obj.to_json());
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else if (option.equals("datos_dir")) {
					String street = request.getParameter("direccion");
					Mapa mp = MapController.get_position(street);
					out.print(mp.to_json());
				} else {
					out.print("ERROR 401");
				}

			} else {
				out.println("ERROR 400");
			}
		} else {
			out.println("ERROR 399");
		}
	}

	private int[] String_to_int_coordenates(String lat, String lon) {
		int new_latitud;
		int new_longitud;
		String latitud = "";
		String longitud = "";
		String[] array_coordenada = lat.split(".");
		String[] array_coordenada1 = lon.split(".");
		// 41.56677
		for (String c : array_coordenada) {
			latitud += c;
		}
		for (String c : array_coordenada1) {
			longitud += c;
		}
		new_latitud = Integer.parseInt(latitud);
		new_longitud = Integer.parseInt(longitud);
		int[] result = { new_latitud, new_longitud };
		return result;
	}

	private int int_coord(String coord) {
		double valor = Double.valueOf(coord);
		// System.out.println("c: " +coord);
		valor = valor * 1000000;

		return (int) Math.floor(valor);

	}

	public Obj_cercanos cercanus(String lat, String lon, int distancia) throws Exception {
		// System.out.println("recibido..." + lat + " " + lon);
		Obj_cercanos obj = new Obj_cercanos();
		// int distancia = 5000;
		

		List<Bicibox> lista_bicibox = BiciboxController.getAll();
		List<Stationsbicing> lista_bicing = StationsbicingController.getAll();
		List<Renfe> lista_renfe = RenfeController.getAll();
		List<TRAM_stops> lista_tram = TramController.get_stops_info();
		List<Metro_lineas> lista_metro = Metro_lineasController.getAll();

		// System.out.println(int_coord(lat) + " | " + int_coord(lon));
		for (Bicibox bicib : lista_bicibox) {
			if ((int_coord(bicib.lat) >= int_coord(lat) - distancia)
					&& (int_coord(bicib.lat) <= int_coord(lat) + distancia)
					&& (int_coord(bicib.longitud) >= int_coord(lon) - distancia * 2)
					&& (int_coord(bicib.longitud) <= int_coord(lon) + distancia * 2)) {
				// System.out.println(bicib.toString());
				obj.List_bicibox.add(bicib);

			} else {
				// System.out.println("NO CUMPLE");
			}
		}
		for (Stationsbicing s : lista_bicing) {
			if ((int_coord(s.latitude) >= int_coord(lat) - distancia)
					&& (int_coord(s.latitude) <= int_coord(lat) + distancia)
					&& (int_coord(s.longitude) >= int_coord(lon) - distancia * 2)
					&& (int_coord(s.longitude) <= int_coord(lon) + distancia * 2)) {
				// System.out.println(s.toString());
				obj.List_stationsbicing.add(s);
			}
		}
		for (Renfe r : lista_renfe) {
			// System.out.println(r.lat + " | " + r.lon);
			if ((int_coord(r.lat) >= int_coord(lat) - distancia) && (int_coord(r.lat) <= int_coord(lat) + distancia)
					&& (int_coord(r.lon) >= int_coord(lon) - distancia * 2)
					&& (int_coord(r.lon) <= int_coord(lon) + distancia * 2)) {
				// System.out.println(r.toString());
				obj.List_renfe.add(r);
			}
		}
		for (TRAM_stops ts : lista_tram) {
			if ((int_coord(ts.latitude) >= int_coord(lat) - distancia)
					&& (int_coord(ts.latitude) <= int_coord(lat) + distancia)
					&& (int_coord(ts.longitude) >= int_coord(lon) - distancia * 2)
					&& (int_coord(ts.longitude) <= int_coord(lon) + distancia * 2)) {
				// System.out.println(ts.toString());
				obj.List_tram.add(ts);
			}
		}
		for (Metro_lineas ml : lista_metro) {
			for (Estaciones e : ml.estations) {
				if ((int_coord(e.ubicacion.Latitud) >= int_coord(lat) - distancia)
						&& (int_coord(e.ubicacion.Latitud) <= int_coord(lat) + distancia)
						&& (int_coord(e.ubicacion.Longitud) >= int_coord(lon) - distancia * 2)
						&& (int_coord(e.ubicacion.Longitud) <= int_coord(lon) + distancia * 2)) {
							obj.List_metro.add(e);
				}
			}
		}
		return obj;
	}

	// protected void doPost(HttpServletRequest request, HttpServletResponse
	// response)
	// throws ServletException, IOException {
	// // create HTML response
	// PrintWriter writer = response.getWriter();
	// writer.println("Peticion doPost...");
	// String app_key = request.getParameter("app_key");
	// String api_key = request.getParameter("api_key");
	// if(api_key != null && app_key != null){

	// if(AppsController.comprovate_credentials(app_key, api_key)){
	// escriture_data(request,"POST",app_key,api_key);
	// // String mail = request.getParameter("email");
	// // String user = request.getParameter("user");
	// // String pass = request.getParameter("pass");
	// // String deve = request.getParameter("developer");

	// // String respuesta = String.format("%s %s %s %s ",mail,user,pass,deve);
	// // writer.println(respuesta);
	// }else{
	// writer.println("ERROR 400");
	// }

	// }else{
	// writer.println("ERROR 399");
	// }

	// }

	private void escriture_data(HttpServletRequest request, String method, String app_key, String api_key) {
		Conexiones con = new Conexiones();
		con.id_apps = AppsController.get_id_por_parametros(app_key, api_key);
		con.type = method;
		con.llamada = request.getRequestURL().toString();
		con.host = request.getRemoteAddr();
		con.idsesion = request.getRequestedSessionId();
		Enumeration<String> e = request.getHeaderNames();
		while (e.hasMoreElements()) {
			String param = (String) e.nextElement();
			if (param.equals("user-agent")) {
				// System.out.println(param + " : " + request.getHeader(param));
				con.maquina = request.getHeader(param);
			}
		}
		String params = "{";
		Map<String, String[]> paramsMap = request.getParameterMap();
		for (String key : paramsMap.keySet()) {
			params += key + " : " + request.getParameter(key) + ",";
			// System.out.println(key + " : " + request.getParameter(key));
		}
		params += "}";
		con.parametros = params;
		ConexionesController.create(con);
	}
}