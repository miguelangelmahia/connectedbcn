<!DOCTYPE html>
<html lang="es">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>ConnectedBCN</title>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
	integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
	<link rel="stylesheet" href="css/estilosNavbar.css">
	<link rel="stylesheet" href="css/estilosIndex.css">
	<link rel="stylesheet" href="css/estilosFooter.css">
</head>

<body>
	<%
		String email = "";
  		String user = "";

  		if (session.getAttribute("email") != null && session.getAttribute("user") != null ) {
  			email = (String) session.getAttribute("email");
            user = (String) session.getAttribute("user");
            %>
      		<%@include file="navbarUsuario.jsp"%>
      		<%
      	} else {
      		%>
      		<%@include file="navbarGeneral.jsp"%>
      		<%
      	}
	%>
	<div id="carouselExampleIndicators"
		class="carousel slide carrusel d-none d-md-block" data-ride="carousel">
		<div class="carousel-inner">
			<div class="carousel-item active">
				<img class="d-block w-100 imagen-carrusel"
					src="imagenes/banner-bicing.jpg" alt="Banner Bicing">
				<div class="carousel-caption d-none d-md-block texto-carrusel">
					<h2>�Llega la nueva era Bicing!</h2>
					<p>Un nuevo servicio con m�s y mejores funcionalidades</p>
				</div>
			</div>
			<div class="carousel-item">
				<img class="d-block w-100 imagen-carrusel"
					src="imagenes/banner-tram.jpg" alt="Banner TRAM">
				<div class="carousel-caption d-none d-md-block texto-carrusel">
					<h2>ECOmu�vete!</h2>
					<p>El TRAM es uno de los medios de transporte m�s sostenibles y
						seguros</p>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="row d-flex justify-content-around contenedor-pregunta">
					<div
						class="col-12 col-md-6 d-flex flex-column align-self-center pregunta">
						<h2 class="text-center">�Qui�nes somos?</h2>
						<p>Somos un grupo de cuatro programadores junior que
							actualmente estamos finalizando nuesto bootcamp de Java & SQL
							impartido por Fundaci�n Esplai.</p>
					</div>
					<div class="col-8 col-sm-6 col-md-4">
						<img alt="Fundaci�n Esplai" src="imagenes/fundacion-esplai.png"
							class="img-fluid imagen-pregunta">
					</div>
				</div>
			</div>

			<div class="col-12">
				<div class="row d-flex justify-content-around contenedor-pregunta">
					<div class="col-8 col-sm-6 col-md-4 order-2 order-md-1">
						<img alt="Metro" src="imagenes/metro.jpg"
							class="img-fluid imagen-pregunta">
					</div>
					<div
						class="col-12 col-md-6 order-1 order-md-2 d-flex flex-column align-self-center pregunta">
						<h2 class="text-center">�Que encontraras aqui?</h2>
						<p>Queremos unificar todos los transportes no contaminantes
							existentes en Barcelona en un solo sitio para que la persona que
							quiera usarlos tenga toda la informacion que necesita lo mas
							accesible posible.</p>
					</div>
				</div>
			</div>

			<div class="col-12">
				<div class="row d-flex justify-content-around contenedor-pregunta">
					<div
						class="col-12 col-md-6 d-flex flex-column align-self-center pregunta">
						<h2 class="text-center">�Que puedes hacer?</h2>
						<p>Al registrarte en nuestra web tendras acceso a toda la
							informacion sobre los transportes mas ecologicos que hay en
							Barcelona, podras guardar tus ubicaciones favoritas, ver las
							opciones que tienes para moverte por la ciudad, los parkings
							libres que tienes disponibles en toda la red de Bicibox y muchas
							mas funcionalidades que estan desarrollo.</p>
					</div>
					<div class="col-8 col-sm-6 col-md-4">
						<img alt="Mapa Bicing" src="imagenes/mapa-bicing.jpg"
							class="img-fluid imagen-pregunta">
					</div>
				</div>
			</div>

			<div class="col-12">
				<div class="row d-flex justify-content-around contenedor-pregunta">
					<div class="col-8 col-sm-6 col-md-4 order-2 order-md-1">
						<img alt="API TRAM" src="imagenes/api-tram.JPG"
							class="img-fluid imagen-pregunta">
					</div>
					<div
						class="col-12 col-md-6 order-1 order-md-2 d-flex flex-column align-self-center pregunta">
						<h2 class="text-center">�De donde sacamos la informacion?</h2>
						<p>Gran parte de la informacion que se muestra la hemos
							obtenido de API's como las de Bicing o TMB, tambien hemos usado
							datasets de Open Data BCN para mostrar informacion estatica.</p>
					</div>
				</div>
			</div>

			<div class="col-12">
				<div class="row d-flex justify-content-around contenedor-pregunta">
					<div
						class="col-12 col-md-6 d-flex flex-column align-self-center pregunta">
						<h2 class="text-center">�Con quien colaboramos?</h2>
						<p>Tenemos el apoyo de Ecologistas en Acci�n, es una
							confederaci�n de m�s de 300 grupos ecologistas distribuidos por
							pueblos y ciudades. Realiza campa�as de sensibilizaci�n,
							denuncias p�blicas o legales contra aquellas actuaciones que
							da�an el medio ambiente, a la vez que elabora alternativas
							concretas y viables en cada uno de los �mbitos en los que
							desarrolla su actividad.</p>
					</div>
					<div class="col-8 col-sm-6 col-md-4">
						<img alt="Ecologistas en acci�n" src="imagenes/logo-ea.png"
							class="img-fluid imagen-pregunta">
					</div>
				</div>
			</div>
		</div>
	</div>
	<%@include file="footer.jsp"%>

	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
</body>

</html>