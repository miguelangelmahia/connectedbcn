<%@page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="es">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>¿Hacia dónde quieres ir?</title>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
	integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
	<link rel="stylesheet" href="css/estilosNavbar.css">
	<link rel="stylesheet" href="css/estilosHaciaDonde.css">
	<link rel="stylesheet" href="css/estilosFooter.css">
	<link rel="stylesheet" href="css/estilosMap.css">
</head>

<body>
	<%
		String email = "";
  		String user = "";

  		if (session.getAttribute("email") != null && session.getAttribute("user") != null ) {
  			email = (String) session.getAttribute("email");
            user = (String) session.getAttribute("user");
            %>
      		<%@include file="navbarUsuario.jsp"%>
      		<%
      	} else {
      		%>
      		<%@include file="navbarGeneral.jsp"%>
      		<%
      	}
	%>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div id="contenedor_position">
				<i class="fas fa-location-arrow"> Localizacion</i>
				<a id="geolocation"></a>
				</div>
				<div  class="leyenda"><img src="img/Leyendas.png" id="imagenleyenda"> </div>
				<div id='myMap'  style='width: 100%; height: 500PX;'></div>
			</div>
		
		</div>
		<div class="row d-flex justify-content-center">
			<div class="col-12 col-sm-9 col-md-6">



				<div class="forma">
					<div class="form-group">
						<label for="direccion"><i class="fas fa-route"></i> Direccion
						</label> <input type="text" class="form-control" id="direccion" placeholder="nombre de calle, numero de la calle, cuidad...">
					</div>

					
					<div class="form-group row">
						<div class="col-sm-10">
							<div class="form-check">
								<input class="form-check-input" type="checkbox" id="gridCheck0">
								<label class="form-check-label" for="gridCheck0">Bicicleta propia</label>
							</div>
							<div class="form-check" id="cargarbicing">
								<input class="form-check-input" type="checkbox" id="gridCheck1">
								<label class="form-check-label" for="gridCheck1">Bicing</label>
							</div>
							<div class="form-check">
								<input class="form-check-input" type="checkbox" id="gridCheck2">
								<label class="form-check-label" for="gridCheck2">Metro/Tram</label>
							</div>
							<div class="form-check">
								<input class="form-check-input" type="checkbox" id="gridCheck3">
								<label class="form-check-label" for="gridCheck3">Renfe</label>
							</div>
							<%-- <div class="form-check">
								<input class="form-check-input" type="checkbox" id="gridCheck4">
								<label class="form-check-label" for="gridCheck4">Caminando</label>
							</div> --%>
							<div class="text-right bt">
							<button id="enviar" type="button" class="btn btn-outline-success">Buscar</button>
							</div>

						</div>
					</div>
					
				</div>


			</div>
		</div>




	</div>






	<%@include file="footer.jsp"%>

	<script type='text/javascript' src='https://www.bing.com/api/maps/mapcontrol?key=Auf4Ic9Y4ZgEO2feZ4X0Gizx1goLhhrNNFKu4tEgmp9Q2GV-N8aXQkOieKftfxyX&callback=gps' async defer></script>
	
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
	<script type="text/javascript" src="js/maps.js"></script>
</body>

</html>