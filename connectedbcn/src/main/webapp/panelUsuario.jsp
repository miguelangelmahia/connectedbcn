<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.codesplai.basedatos.*" %>
<%@page import="java.util.*" %>
<!DOCTYPE html>
<html lang="es">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Panel Control</title>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
	integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
	<link rel="stylesheet" href="css/estilosNavbar.css">
	<link rel="stylesheet" href="css/estilosPanelUsuario.css">
	<link rel="stylesheet" href="css/estilosFooter.css">
	
</head>
<body>

    <%
		String email = "";
  		String user = "";
        Usuarios us = null;
  		if (session.getAttribute("email") != null && session.getAttribute("user") != null ) {
  			email = (String) session.getAttribute("email");
            user = (String) session.getAttribute("user");
            us = UsuariosController.getUsuarioByEmail(email);
            %>
      		<%@include file="navbarUsuario.jsp"%>
      		<%
      	} else {
      		%>
      		<%@include file="navbarGeneral.jsp"%>

              
      		<%
      	}
	%>

    <div class="container">
        <div class="row">
            <div class="col 12 title">
                <h1><a><%=user%></a></h1>
            </div>
        </div>
    </div>

<%if(us.developer){%>
    <div class="container">
        <div class="row contenapps">
            <div class="col 6">
                <h1>Mis Apps</h1>
                <table class=" table pruebatabla">
                    <thead class="thead">
                        <tr>
                            <th>Nombre</th>
                            <th>Api_key</th>
                            <th></th>
                        </tr>
                    </thead>

                    <%for(Apps e : AppsController.getAll_user(us)){%>
                    <tr>
                        <td><%=e.nombre%></td>
                        <td><%=e.apikey%></td>
                        <td><button class="btn btn-success" onclick="llamada_del_app(<%=e.id%>,<%=e.id_usuario%>)">Borrar</button></td>
                    </tr>
                    <%}%>
                </table>
            </div>

            <div class="col 6 text-center">
                <h4>Nueva App</h4>
                <button id="new_app" type="button" class="btn btn-success">+</button> Agregar app

                <div id="in_app" class="forma text-right p-3 d-none">
                    <input  class="form-control" type="text" placeholder="Nombre de la aplicación">
                    <button id="create_app" type="button" class="btn btn-success m-3">Crear</button>
                </div>
            </div>
        </div>
    </div>
<%}else{%>
        <div class="container">
            <div class="row contenapps2">
                <div class="col 12 text-center">
                    <div>
                        <h1 class="forma ">No tiene permisos de desarrollador.</h1>
                    </div>
                    <div class="forma "> Para obtener permisos ingresa en <a href="perfilUsuario.jsp" class="btn boton-menu btn btn-outline-success">Ver perfil <i
                        class="fas fa-user-circle"></i></a> y apúntate como desarrollador.</div>
                </div>
            </div>
        </div>


<%}%>

	<%@include file="footer.jsp"%>

	<script type='text/javascript' src='https://www.bing.com/api/maps/mapcontrol?key=Auf4Ic9Y4ZgEO2feZ4X0Gizx1goLhhrNNFKu4tEgmp9Q2GV-N8aXQkOieKftfxyX&callback=gps' async defer></script>
	
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>

    <script>
    var globalEmail="<%=email%>";
 </script>
    <script type="text/javascript" src="js/panelUser.js"></script>
</body>