var contrasenaNuevaOk = false;
var confirmaContrasenaNuevaOk = false;
var telefonoOk = false;
var fechaNacimientoOk = false;
var desarrolladorOk = false;

$("input#contrasenaNueva")
		.on(
				"input",
				function() {

					var contrasena = $("input#contrasenaNueva").val();

					if (contrasena
							.match("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]{8,}$")) {
						$("#informacionContrasenaNueva").text(
								"Contraseña válida");
						$("#informacionContrasenaNueva").removeClass(
								"informacion-negativa");
						contrasenaNuevaOk = true;
					} else {
						$("#informacionContrasenaNueva")
								.text(
										"La contraseña debe tener una minúscula, una mayúscula, un número y una longitud de ocho caracteres como mínimo");
						$("#informacionContrasenaNueva").addClass(
								"informacion-negativa");
						contrasenaNuevaOk = false;
					}

					if (contrasenaNuevaOk === true
							&& confirmaContrasenaNuevaOk === true) {
						$("#botonGuardar").removeAttr("disabled");
					} else {
						$("#botonGuardar").attr("disabled", "disabled");
					}
				});

$("input#confirmaContrasenaNueva").on(
		"input",
		function() {

			var contrasena = $("input#contrasenaNueva").val();
			var confirmaContrasena = $("input#confirmaContrasenaNueva").val();

			if (confirmaContrasena === contrasena) {
				$("#informacionConfirmaContrasenaNueva").text(
						"Las contraseñas coinciden");
				$("#informacionConfirmaContrasenaNueva").removeClass(
						"informacion-negativa");
				confirmaContrasenaNuevaOk = true;
			} else {
				$("#informacionConfirmaContrasenaNueva").text(
						"Las contraseñas no coinciden");
				$("#informacionConfirmaContrasenaNueva").addClass(
						"informacion-negativa");
				confirmaContrasenaNuevaOk = false;
			}

			if (contrasenaNuevaOk === true
					&& confirmaContrasenaNuevaOk === true) {
				$("#botonGuardar").removeAttr("disabled");
			} else {
				$("#botonGuardar").attr("disabled", "disabled");
			}
		});

$("input#telefono").on("input", function() {

	var telefono = $("input#telefono").val();

	if (telefono.match("^[0-9]{9}$")) {
		$("#informacionTelefono").text("Teléfono válido");
		$("#informacionTelefono").removeClass("informacion-negativa");
		telefonoOk = true;
	} else {
		$("#informacionTelefono").text("Introduce un teléfono válido");
		$("#informacionTelefono").addClass("informacion-negativa");
		telefonoOk = false;
	}

	if (telefonoOk === true) {
		$("#botonGuardar").removeAttr("disabled");
	} else {
		$("#botonGuardar").attr("disabled", "disabled");
	}
});

$("input#fechaNacimiento").on("input", function() {

	var fechaNacimiento = $("input#fechaNacimiento").val();

	if (fechaNacimiento !== "") {
		fechaNacimientoOk = true;
	} else {
		fechaNacimientoOk = false;
	}

	if (fechaNacimientoOk === true) {
		$("#botonGuardar").removeAttr("disabled");
	} else {
		$("#botonGuardar").attr("disabled", "disabled");
	}
});

//$("input#desarrollador").on("input", function() {
//	if ($('#desarrollador').prop('checked')) {
//		desarrolladorOk = true;
//	} else {
//		desarrolladorOk = false;
//	}
//
//	if (desarrolladorOk === true) {
//		$("#botonGuardar").removeAttr("disabled");
//	} else {
//		$("#botonGuardar").attr("disabled", "disabled");
//	}
//});