var usuarioOk = false;
var emailOk = false;
var contrasenaOk = false;
var confirmaContrasenaOk = false;

function validarRegistro() {
	if (usuarioOk === true && emailOk === true && contrasenaOk === true && confirmaContrasenaOk === true) {
		$("#botonRegistro").removeAttr("disabled");
	}else{
		$("#botonRegistro").attr("disabled", "disabled");
	}
}

$("input#usuario").on("input", function() {

	var usuario = $("input#usuario").val();
	
	if (usuario.match("^[a-zA-Z0-9]{4,}$")) {
		$("#informacionUsuario").text("Nombre de usuario válido");
		$("#informacionUsuario").removeClass("informacion-negativa");
		usuarioOk = true;
	} else {
		$("#informacionUsuario").text("Solo se aceptan números y letras con una longitud mínima de cuatro caracteres");
		$("#informacionUsuario").addClass("informacion-negativa");
		usuarioOk = false;
	}
	validarRegistro();
});

$("input#email").on("input", function() {

	var email = $("input#email").val();

	if (email.match("^.*@(gmail|hotmail|outlook|yahoo).(com|es|org)$")) {
		$("#informacionEmail").text("Email válido");
		$("#informacionEmail").removeClass("informacion-negativa");
		emailOk = true;
	} else {
		$("#informacionEmail").text("Introduce un email válido");
		$("#informacionEmail").addClass("informacion-negativa");
		emailOk = false;
	}
	validarRegistro();
});

$("input#contrasena").on("input", function() {
		
	var contrasena = $("input#contrasena").val();

	if (contrasena.match("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]{8,}$")) {
		$("#informacionContrasena").text("Contraseña válida");
		$("#informacionContrasena").removeClass("informacion-negativa");
		contrasenaOk = true;
	} else {
		$("#informacionContrasena").text("La contraseña debe tener una minúscula, una mayúscula, un número y una longitud de ocho caracteres como mínimo");
		$("#informacionContrasena").addClass("informacion-negativa");
		contrasenaOk = false;
	}
	validarRegistro();
});

$("input#confirmaContrasena").on("input", function() {
		
	var contrasena = $("input#contrasena").val();
	var confirmaContrasena = $("input#confirmaContrasena").val();

	if (confirmaContrasena === contrasena) {
		$("#informacionConfirmaContrasena").text("Las contraseñas coinciden");
		$("#informacionConfirmaContrasena").removeClass("informacion-negativa");
		confirmaContrasenaOk = true;
	} else {
		$("#informacionConfirmaContrasena").text("Las contraseñas no coinciden");
		$("#informacionConfirmaContrasena").addClass("informacion-negativa");
		confirmaContrasenaOk = false;
	}
	validarRegistro();
});
