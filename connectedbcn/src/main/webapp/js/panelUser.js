$("#new_app").click(function() {
    var listSort = $("#in_app");
    // listSort.toggleClass("d-none");
    if (listSort.hasClass('d-none')) {
        listSort.removeClass('d-none');
        $("#new_app").text("-");
    } else {
        listSort.addClass("d-none");
        $("#new_app").text("+");
    }
});
// http://localhost:8080/connectedbcn/api/aplications?app_key=cnbcn&api_key=123456&nombre=juanka&email=jkindutrias@gmail.com
$("#create_app").click(function(){
    var name_app = $("#in_app input").val();
    var email_use = globalEmail;
    console.log("click: " + name_app);
    llamada_new_app(name_app,email_use);
});

function llamada_del_app(id,id_us){
    // console.log(id + " || " + id_us)
    var url = "api/aplications";
    var datos = {
        app_key:"cnbcn",
        api_key:"123456",
        id:id,
        option:"del",
        id_user:id_us
    }
    $.get(url,datos,function(data){
        console.log(data);
        location.reload();
    });
}

function llamada_new_app(ap,emi) {
    var url = "api/aplications";
    var datos = {
        app_key:"cnbcn",
        api_key:"123456",
        nombre:ap,
        option:"add",
        email:emi
    }
    $.get(url,datos,function(data){
        console.log(data);
        location.reload();
    });
}