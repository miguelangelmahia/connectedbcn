<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@page import="com.codesplai.basedatos.*"%>
	<%
	String email = request.getParameter("email");
	String contrasena = request.getParameter("contrasena");
	String error = "";
	boolean usuarioOk = false;
	if (email != null && contrasena != null) {
		usuarioOk = UsuariosController.validarInicioSesion(email, contrasena);
		if(usuarioOk == true){
			Usuarios us = UsuariosController.getUsuarioByEmail(email);
			session.setAttribute("email",us.getEmail());
            session.setAttribute("user",us.getUser());
	        response.sendRedirect(request.getContextPath() + "/index.jsp");
		}else{
			error = "Email o contraseña incorrecto";
		}
	}
	%>
<!DOCTYPE html>
<html lang="es">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Inicio de sesión</title>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
	integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
	<link rel="stylesheet" href="css/estilosNavbar.css">
	<link rel="stylesheet" href="css/estilosInicioSesion.css">
	<link rel="stylesheet" href="css/estilosFooter.css">
</head>

<body>
	<%@include file="navbarGeneral.jsp"%>
	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="col-12 col-sm-9 col-md-6">
				<h2 class="text-center">Inicio de sesíon</h2>
				<p class="error"><%=error%></p>
				<form action="inicioSesion.jsp" method="POST">
					<div class="form-group">
						<label for="email"><i class="fas fa-envelope"></i> Email</label> <input
							type="email" class="form-control" id="email" name="email"
							placeholder="Introduce tu email...">
							<span class="informacion-input informacion-negativa"
							id="informacionEmail"></span>
					</div>
					<div class="form-group">
						<label for="contrasena"><i class="fas fa-key"></i>
							Contraseña</label> <input type="password" class="form-control"
							id="contrasena" name="contrasena" placeholder="Introduce tu contraseña...">
							<span class="informacion-input informacion-negativa" id="informacionContrasena"></span>
					</div>
					<button class="btn btn-block boton-inicio-sesion" id="botonInicioSesion" type="submit">Iniciar
						sesión</button>
					<p class="text-center">
						¿Eres nuevo en ConnectedBCN? <a href="registro.jsp">Registrate
							<i class="fas fa-user-plus"></i>
						</a>
					</p>
				</form>
			</div>
		</div>
	</div>
	<%@include file="footer.jsp"%>

	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
</body>

</html>