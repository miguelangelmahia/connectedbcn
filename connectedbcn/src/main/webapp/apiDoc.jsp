<%@page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="es">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>API Doc</title>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
	integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
	<link rel="stylesheet" href="css/estilosNavbar.css">
	<link rel="stylesheet" href="css/estilosDocs.css">
	<link rel="stylesheet" href="css/estilosFooter.css">
	
</head>

<body>
<%
		String email = "";
  		String user = "";

  		if (session.getAttribute("email") != null && session.getAttribute("user") != null ) {
  			email = (String) session.getAttribute("email");
            user = (String) session.getAttribute("user");
            %>
      		<%@include file="navbarUsuario.jsp"%>
      		<%
      	} else {
      		%>
      		<%@include file="navbarGeneral.jsp"%>
      		<%
      	}
%>

    <div class="container">
        <div class="row">
            <div class="col 12">
                <h1 class="m-5">API rest </h1>
                <h3 class="titulos">Url: /connectedbcn/api/transporte</h3>
                <br>
                <pre class="code">
PETICION ->               
                METHOD: 
                            GET
                PARAMETERS:
                            app_key:[name app]
                            api_key:[key app]
                            option:[cercania]
                            longitud:[coordenada.longitud]
                            latitud:[coordenada.latitud]
                </pre>
                
                <pre class="code">
RESPUESTA ->               
                </pre>
            </div>
        </div>
        <br>
         <div class="row">
            <div class="col 12">
                <pre class="code">
PETICION ->               
                METHOD: 
                            GET
                PARAMETERS:
                            app_key:[name app]
                            api_key:[key app]
                            option:[datos_dir]
                            direccion:[direccion]
                            
                </pre>
                <pre class="code">
RESPUESTA ->               
                </pre>
            </div>
        </div>
    </div>



    <%@include file="footer.jsp"%>

	<script type='text/javascript' src='https://www.bing.com/api/maps/mapcontrol?key=Auf4Ic9Y4ZgEO2feZ4X0Gizx1goLhhrNNFKu4tEgmp9Q2GV-N8aXQkOieKftfxyX&callback=gps' async defer></script>
	
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
	
</body>
