<%@page contentType="text/html; charset=UTF-8" %>
<nav class="navbar navbar-expand-lg menu">
	<a class="navbar-brand" href="index.jsp">ConnectedBCN</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<i class="fas fa-bars menu-comprimido"></i>
	</button>

	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item active"><a class="nav-link link-menu"
				href="index.jsp">Inicio <span class="sr-only">(current)</span>
			</a></li>
			<li class="nav-item"><a class="nav-link link-menu"
				href="haciaDonde.jsp">¿Hacia dónde quieres ir?</a></li>
			<li class="nav-item"><a class="nav-link link-menu"
				href="apiDoc.jsp">API doc</a></li>
		</ul>
		<a href="inicioSesion.jsp" class="btn boton-menu">Entra <i
			class="fas fa-sign-in-alt"></i></a> <a href="registro.jsp"
			class="btn boton-menu">Regístrate <i class="fas fa-user-plus"></i></a>
	</div>
</nav>