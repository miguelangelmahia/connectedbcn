<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@page import="com.codesplai.basedatos.*"%>
	<%
	String cerrarSesion = request.getParameter("cerrarSesion");
	if (cerrarSesion != null) {
		session.invalidate(); 
	    response.sendRedirect(request.getContextPath() + "/index.jsp");
	}
	%>