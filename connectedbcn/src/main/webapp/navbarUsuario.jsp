<%@page contentType="text/html; charset=UTF-8" %>
<nav class="navbar navbar-expand-lg menu">
	<a class="navbar-brand" href="index.jsp">ConnectedBCN</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<i class="fas fa-bars menu-comprimido"></i>
	</button>

	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item active"><a class="nav-link link-menu"
				href="index.jsp">Inicio <span class="sr-only">(current)</span>
			</a></li>
			<li class="nav-item"><a class="nav-link link-menu"
				href="haciaDonde.jsp">¿Hacia dónde quieres ir?</a></li>
			<li class="nav-item"><a class="nav-link link-menu"
				href="panelUsuario.jsp">Mis aplicaciones</a></li>
			<li class="nav-item"><a class="nav-link link-menu"
				href="apiDoc.jsp">API doc</a></li>
		</ul>
		<a href="perfilUsuario.jsp" class="btn boton-menu">Ver perfil <i
			class="fas fa-user-circle"></i></a>
			<form class="form-cerrar-sesion" action="cerrarSesion.jsp" method="POST">
                <input type="hidden" name="cerrarSesion" value="salir">
                <button class="btn boton-cerrar-sesion" type="submit">
				Cerrar sesión <i class="fas fa-sign-out-alt"></i></button>
            </form>		
	</div>
</nav>