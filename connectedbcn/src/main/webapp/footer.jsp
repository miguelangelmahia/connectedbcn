<footer class="container-fluid">
	<div class="row">
		<div class="col-12 redes-sociales">
			<div class="row d-flex justify-content-around">
				<div class="col-3 col-sm-2 d-flex justify-content-center">
					<div
						class="facebook d-flex justify-content-around align-items-center">
						<i class="fab fa-facebook-f"></i>
					</div>
				</div>
				<div class="col-3 col-sm-2 d-flex justify-content-center">
					<div
						class="google-plus d-flex justify-content-around align-items-center">
						<i class="fab fa-google-plus-g"></i>
					</div>
				</div>
				<div class="col-3 col-sm-2 d-flex justify-content-center">
					<div
						class="instagram d-flex justify-content-around align-items-center">
						<i class="fab fa-instagram"></i>
					</div>
				</div>
				<div class="col-3 col-sm-2 d-flex justify-content-center">
					<div
						class="twitter d-flex justify-content-around align-items-center">
						<i class="fab fa-twitter"></i>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12">
			<div class="row">
				<div class="col-12 col-md-6 text-center contacto text-center">
					<p>
						Ponte en contacto con nosotros pulsando <a href="#">aqu�</a>.
					</p>
				</div>
				<div class="col-12 col-md-6 text-center copyright text-center">
					<p>@Copyright 2019 ConnectedBCN</p>
				</div>
			</div>
		</div>
	</div>
</footer>