<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@page import="com.codesplai.basedatos.*"%>
<!DOCTYPE html>
<html lang="es">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Perfil de Usuario</title>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
	integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
	<link rel="stylesheet" href="css/estilosNavbar.css">
	<link rel="stylesheet" href="css/estilosPerfilUsuario.css">
	<link rel="stylesheet" href="css/estilosFooter.css">
</head>

<body>
	<%
		String email = "";
  		String user = "";
  		
  	  
          

  		if (session.getAttribute("email") != null && session.getAttribute("user") != null ) {
  			email = (String) session.getAttribute("email");
            user = (String) session.getAttribute("user");
      		Usuarios us = UsuariosController.getUsuarioByEmail(email);
      		
      		if ("POST".equalsIgnoreCase(request.getMethod())) {
    	  		String contrasenaNueva = request.getParameter("contrasenaNueva");
    	  		if (contrasenaNueva != null && contrasenaNueva.length() > 0) {
    	  			UsuariosController.updateContrasena(email, contrasenaNueva);
    	  		}
    	  		
    	  		String telefono = request.getParameter("telefono");
    	  		if (telefono != null && telefono.length() > 0) {
    	  			UsuariosController.updateTelefono(email, telefono);
    	  		}
    	  		
    	  		String fechaNacimiento = request.getParameter("fechaNacimiento");
    	  		if (fechaNacimiento != null && fechaNacimiento.length() > 0) {
    	  			UsuariosController.updateFechaNacimiento(email, fechaNacimiento);
    	  		}
    	  		
    	  		String desarrollador = request.getParameter("desarrollador");
    	  		if (desarrollador != null) {
    	  			UsuariosController.updateDesarrollador(email, 1);
    	  			/*String apikey = UsuariosController.generadorApiKey();
    	  			boolean existeApiKey = false;
    	  			
    	  			do{
    	  				apikey = UsuariosController.generadorApiKey();
    	  				existeApiKey = UsuariosController.existeApiKey(apikey);
    	  			} while(existeApiKey != false);
    	  			UsuariosController.insertApiKey(apikey, us);*/	
    	  		} else{
    	  			UsuariosController.updateDesarrollador(email, 0);
    	  			UsuariosController.updateApiKey("" , us);
    	  		}
    	  	  }
      		
      		us = UsuariosController.getUsuarioByEmail(email);
      	  	
            %>
      		<%@include file="navbarUsuario.jsp"%>
      		<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="col-12 col-sm-9 col-md-6">
				<h2 class="text-center">Mi Perfil</h2>
				<form class="datosUsuario" action="perfilUsuario.jsp" method="POST">
					<div class="form-group">
						<label for="email"><i class="fas fa-envelope"></i> Email</label> <input
							type="email" class="form-control" id="email" name="email"
							value="<%=email%>" disabled>
					</div>
					<div class="form-group">
						<label for="usuario"><i class="fas fa-user"></i>
							Usuario</label> <input type="text" class="form-control"
							id="usuario" name="usuario" value="<%=user%>" disabled>
					</div>
					<div class="form-group">
						<label for="contrasenaNueva"><i class="fas fa-key"></i>
							Contraseña nueva</label> <input type="password" class="form-control"
							id="contrasenaNueva" name="contrasenaNueva" placeholder="Introduce tu contraseña nueva...">
							<span class="informacion-input informacion-negativa" id="informacionContrasenaNueva"></span>
					</div>
					<div class="form-group">
						<label for="confirmaContrasenaNueva"><i class="fas fa-key"></i>
							Confirma tu contraseña nueva</label> <input type="password" class="form-control"
							id="confirmaContrasenaNueva" name="confirmaContrasenaNueva" placeholder="Confirma tu contraseña nueva...">
							<span class="informacion-input informacion-negativa" id="informacionConfirmaContrasenaNueva"></span>
					</div>
					<div class="form-group">
						<label for="telefono"><i class="fas fa-phone"></i>
							Teléfono</label> <input type="text" class="form-control"
							id="telefono" name="telefono" value="<%=us.telefono%>">
							<span class="informacion-input informacion-negativa" id="informacionTelefono"></span>
					</div>
					<div class="form-group">
						<label for="fechaNacimiento"><i class="far fa-calendar-alt"></i>
							Fecha de nacimiento</label> <input type="date" class="form-control"
							id="fechaNacimiento" name="fechaNacimiento" value="<%=us.nacimiento%>">
							<span class="informacion-input informacion-negativa" id="informacionFechaNacimiento"></span>
					</div>
					<div class="form-check">
  						<input class="form-check-input" type="checkbox" id="desarrollador" name="desarrollador" <% if(us.developer == true){out.println("checked='checked'");}%>>
  						<label class="form-check-label" for="desarrollador">
    					<i class="fas fa-laptop-code"></i> ¿Eres desarrollador?
  						</label>
					</div>
					<button class="btn btn-block boton-guardar" id="botonGuardar" type="submit" disabled>Guardar</button>
				</form>
			</div>
		</div>
	</div>
      		<%
      	} else {
      		%>
      		<%@include file="navbarGeneral.jsp"%>
      		<h2 class="text-center">No has iniciado sesión</h2>
      		<%
      	}	  
	%>
	<%@include file="footer.jsp"%>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
	<script type="text/javascript" src="js/validarCambiosPerfil.js"></script>
</body>
</html>