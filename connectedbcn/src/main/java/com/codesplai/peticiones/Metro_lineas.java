package com.codesplai.peticiones;

import java.util.*;

public class Metro_lineas {

   public int _codi_familia;
   public int _codi_linia;
   public String _codi_tipus_calendari;
   public String _color_aux_linia;
   public String _color_linia;
   public String _data;
   public String _desc_linia;
   public String _desti_linia;
   public int _id_linia;
   public int _id_operador;
   public String _nom_familia;
   public String _nom_linia;
   public String _nom_operador;
   public String _nom_tipus_calendari;
   public String _nom_tipus_transport;
   public int _num_paquets;
   public int _ordre_familia;
   public int _ordre_linia;
   public String _origin_linea;
   public List<Coordinates> dibujo_linea;
   public List<Estaciones> estations;

   public Metro_lineas() {
      this.dibujo_linea= new ArrayList<Coordinates>();
      this.estations = new ArrayList<Estaciones>();
   
   }

   public void set_Lista_estation(List<Estaciones> e) {
      estations = e;
   }

   public void set_estation(Estaciones e) {
      estations.add(e);
   }

   public void set_dibujo_linea(Coordinates c) {
      dibujo_linea.add(c);
      // System.out.println("Coordenada creada: " + c.toString());
   }

   @Override
   public String toString() {
      return String.format("%d %d %s %s %s %s %s %s %d %d %s %s %s %s %s %d %d %d %s", this._codi_familia,
            this._codi_linia, this._codi_tipus_calendari, this._color_aux_linia, this._color_linia, this._data,
            this._desc_linia, this._desti_linia, this._id_linia, this._id_operador, this._nom_familia, this._nom_linia,
            this._nom_operador, this._nom_tipus_calendari, this._nom_tipus_transport, this._num_paquets,
            this._ordre_familia, this._ordre_linia, this._origin_linea);
   }

}