package com.codesplai.peticiones;

public class Extrabicing {
    public Boolean has_ebikes;
    public Boolean online;
    public int uid;

    // constructor con todos los atributos
    public Extrabicing(String has_ebikes, String online, int uid) {
        if (has_ebikes.equals("true")) {
            this.has_ebikes = true;
        } else {
            this.has_ebikes = false;
        }

        if (online.equals("true")) {

            this.online = true;
        } else {
            this.online = false;
        }
        this.uid = uid;
    }

    @Override
    public String toString() {
        return String.format("has_ebikes:%b online:%b uid:%d", this.has_ebikes, this.online, this.uid);
    }
}