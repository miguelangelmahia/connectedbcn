package com.codesplai.peticiones;



public class Stationsbicing {
    public int empty_slots;
    public int free_bikes;
    public String latitude;
    public String longitude;
    public String id;
    public String name;
    public String timestamp;
    public Extrabicing estado;
    // constructor con todos los atributos
    public Stationsbicing(int empty_slots, int free_bikes, String latitude, String longitude, String id,
            String name, String timestamp, Extrabicing estado) {
        this.empty_slots = empty_slots;
        this.free_bikes = free_bikes;
        this.latitude = latitude;
        this.longitude = longitude;
        this.id = id;
        this.name = name;
        this.timestamp = timestamp;
        this.estado = estado;
    }

    // Constructor sin id, ni timestramp
    public Stationsbicing(int empty_slots, int free_bikes, String latitude, String longitude, String name) {
        this.empty_slots = empty_slots;
        this.free_bikes = free_bikes;
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
    }
    @Override
    public String toString() {
        return String.format(
                "empty_slots:%d free_bikes:%d latitude:%s longitude:%s id:%s name:%s timestamp:%s estado:%s",
                this.empty_slots, this.free_bikes, this.latitude, this.longitude, this.id, this.name,
                this.timestamp, this.estado.toString());
    }
    public Stationsbicing(){}
}