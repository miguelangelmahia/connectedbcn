package com.codesplai.peticiones;

public class TRAM_stops {
public String name;
public String latitude;
public String longitude;
public int outboundCode;
public int returnCode;
public String gtfsCode;
public String image;
public int id;

//constructor vacio
public TRAM_stops(){  
}

//constructor con todo
public TRAM_stops(String name, String latitude, String longitude, int outboundCode, int returnCode, String gtfsCode, String image, int id){
    this.name= name;
    this.latitude= latitude;
    this.longitude= longitude;
    this.outboundCode= outboundCode;
    this.returnCode= returnCode;
    this.gtfsCode= gtfsCode;
    this.image= image;
    this.id= id;
}
@Override
public String toString() {
    return String.format(
            "name:%s latitude:%s longitude:%s outboundCode:%d returnCode:%d gtfsCode:%s image:%s id:%d ",
            this.name, this.latitude, this.longitude, this.outboundCode, this.returnCode, this.gtfsCode, this.image, this.id);
}
}