package com.codesplai.peticiones;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.Gson;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class StationsbicingController {

    public static List<Stationsbicing> getAll() throws IOException {

        List<Stationsbicing> sb = new ArrayList<Stationsbicing>();

        System.setProperty("http.proxyHost", "...");
        System.setProperty("http.proxyPort", "8080");
        URL url = new URL("https://api.citybik.es/v2/networks/bicing");
        URLConnection conn = url.openConnection();
        InputStream is = conn.getInputStream();
        BufferedInputStream bis = new BufferedInputStream(is);
        String strFileContents = "";
        try {

            byte[] contents = new byte[1024];

            int bytesRead = 0;

            while ((bytesRead = bis.read(contents)) != -1) {
                strFileContents += new String(contents, 0, bytesRead);
            }

            Gson gson = new Gson();
            JsonElement element = gson.fromJson(strFileContents, JsonElement.class);
            JsonObject jsonObj = element.getAsJsonObject();
            JsonObject networkObject = jsonObj.getAsJsonObject("network");
            JsonArray estaciones = networkObject.getAsJsonArray("stations");


            for (JsonElement e : estaciones) {
                Stationsbicing es = new Stationsbicing();
                JsonObject estacionactual = e.getAsJsonObject();
                es.empty_slots= Integer.parseInt(estacionactual.get("empty_slots").toString());
                es.free_bikes= Integer.parseInt(estacionactual.get("free_bikes").toString());
                es.id= estacionactual.get("id").toString();
                es.latitude= estacionactual.get("latitude").toString();
                es.longitude= estacionactual.get("longitude").toString();
                es.name= estacionactual.get("name").toString();
                es.name=es.name.split("\"")[1];
                es.timestamp= estacionactual.get("timestamp").toString();
                JsonObject estado = estacionactual.getAsJsonObject("extra");
                Extrabicing ex = new Extrabicing(estado.get("has_ebikes").toString(),estado.get("online").toString(),   Integer.parseInt(  estado.get("uid").toString()  )  );
                es.estado = ex;
                sb.add(es);
                
                

            } 
        } catch (Exception e) {
            System.out.println(e.toString());

        }
        return sb;
    }
}