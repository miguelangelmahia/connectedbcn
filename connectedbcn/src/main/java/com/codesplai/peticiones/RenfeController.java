package com.codesplai.peticiones;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.Gson;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class RenfeController {

    public static List<Renfe> getAll() throws IOException {

        List<Renfe> rf = new ArrayList<Renfe>();

        System.setProperty("http.proxyHost", "...");
        System.setProperty("http.proxyPort", "8080");
        URL url = new URL("http://barcelonaapi.marcpous.com/renfe/stations.json");
        URLConnection conn = url.openConnection();
        InputStream is = conn.getInputStream();
        BufferedInputStream bis = new BufferedInputStream(is);
        String strFileContents = "";
        try {

            byte[] contents = new byte[1024];

            int bytesRead = 0;

            while ((bytesRead = bis.read(contents)) != -1) {
                strFileContents += new String(contents, 0, bytesRead);
            }
            Gson gson = new Gson();
            JsonElement element = gson.fromJson(strFileContents, JsonElement.class);
            JsonObject jsonObj = element.getAsJsonObject();
            JsonObject jsonObjc = jsonObj.getAsJsonObject("data"); 
            JsonArray arrayElementos = jsonObjc.getAsJsonArray("renfe");
            // System.out.println(arrayElementos);;
            for(JsonElement r: arrayElementos){
                Renfe ren  = new Renfe();
                JsonObject obj = r.getAsJsonObject();
                ren.id= obj.get("id").toString();
                ren.lat= obj.get("lat").toString();
                ren.lat= ren.lat.split("\"")[1];
                ren.line=obj.get("line").toString();
                ren.line=ren.line.split("\"")[1];
                ren.lon= obj.get("lon").toString();
                ren.lon= ren.lon.split("\"")[1];
                ren.name= obj.get("name").toString();
                ren.name=ren.name.split("\"")[1];
                ren.operator = obj.get("operator").toString();
                ren.operator=ren.operator.split("\"")[1];
                ren.type= obj.get("type").toString();
                ren.type=ren.type.split("\"")[1];
                ren.zone= obj.get("zone").toString();
               rf.add(ren);
            }

        } catch (Exception e) {
            System.out.println(e.toString());
            
        }
        // System.out.println(rf);
        return rf;
    }
}