package com.codesplai.peticiones;

public class Bicibox {
    public int buits;
    public String direccio;
    public int distancia;
    public int id;
    public String lat;
    public String longitud;
    public String municipi;
    public String nom;
    public int plens;
    public Boolean preferit;

    public Bicibox() {
        
    }

    // constructor completo
    public Bicibox(int buits, String direccio, int distancia, int id, String lat, String longitud, String municipi,
            String nom, int plens, Boolean preferit) {
        this.buits = buits;
        this.direccio = direccio;
        this.distancia = distancia;
        this.id = id;
        this.lat = lat;
        this.longitud = longitud;
        this.municipi = municipi;
        this.nom = nom;
        this.plens = plens;
        this.preferit = preferit;
    }

    @Override
    public String toString() {
        return String.format(
                "libres:%d direccion:%s distancia:%d id:%s latitud:%s longitud:%s municipio:%s nombre:%s ocupadas:%d preferti:%b ",
                this.buits, this.direccio, this.distancia, this.id, this.lat, this.longitud, this.municipi, this.nom,
                this.plens, this.preferit);
    }

}