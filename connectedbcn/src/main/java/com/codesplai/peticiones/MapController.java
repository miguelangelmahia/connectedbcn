package com.codesplai.peticiones;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import com.google.gson.Gson;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;


public class MapController{


    public static Mapa get_position(String street) throws IOException{
        Mapa mp = new Mapa();
        // http://dev.virtualearth.net/REST/v1/Locations/ + street + ?o=json&key=Auf4Ic9Y4ZgEO2feZ4X0Gizx1goLhhrNNFKu4tEgmp9Q2GV-N8aXQkOieKftfxyX
        String urls = " http://dev.virtualearth.net/REST/v1/Locations/" + street + "?o=json&key=Auf4Ic9Y4ZgEO2feZ4X0Gizx1goLhhrNNFKu4tEgmp9Q2GV-N8aXQkOieKftfxyX";
       
        URL url = new URL(urls);
        URLConnection conn = url.openConnection();
        InputStream is = conn.getInputStream();
        BufferedInputStream bis = new BufferedInputStream(is);
        String strFileContents = "";
        try {

            byte[] contents = new byte[1024];
            int bytesRead = 0;

            while ((bytesRead = bis.read(contents)) != -1) {
                strFileContents += new String(contents, 0, bytesRead);
            }

            Gson gson = new Gson();
            JsonElement element = gson.fromJson(strFileContents, JsonElement.class);
            JsonObject jsonObj = element.getAsJsonObject();

            JsonArray arrayElementos = jsonObj.getAsJsonArray("resourceSets");

            JsonElement Son = arrayElementos.get(0);

            JsonObject objSon = Son.getAsJsonObject();
            JsonArray array1 = objSon.getAsJsonArray("resources");

            JsonElement Son1 = array1.get(0);

            JsonObject obj = Son1.getAsJsonObject();
            
            

            JsonElement elem = obj.get("point");

            JsonObject objel = elem.getAsJsonObject();

            JsonArray arre = objel.getAsJsonArray("coordinates");

            mp.nombre = obj.get("name").toString();
            // System.out.println(obj.get("name"));
            Coordinates co = new Coordinates(arre.get(1).toString(), arre.get(0).toString());
            mp.coordenadas = co;
            // System.out.println(arre.get(0));
            // System.out.println(arre.get(1));


            // System.out.println(objel.get("addressLine"));

        } catch (Exception e) {
            System.out.println(e.toString());
        }
        // System.out.println(strFileContents);
        return mp;
        
    }


}