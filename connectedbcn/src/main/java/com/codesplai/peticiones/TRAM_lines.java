package com.codesplai.peticiones;

public class TRAM_lines {
    public String name;
    public int code;
    public String image;
    public int id;
    TRAM_stops paradas;

    // constructor vacio
    public TRAM_lines() {
    }

    // constructor con todo
    public TRAM_lines(String name, int code, String image, int id) {
        this.name = name;
        this.code = code;
        this.image = image;
        this.id = id;
    }

    @Override
    public String toString() {
        return String.format("name:%s  code:%d image:%s id:%d ", this.name, this.code, this.image, this.id);
    }
}