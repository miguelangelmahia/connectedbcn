package com.codesplai.peticiones;

import java.io.*;
import java.net.*;
import java.util.*;
import com.codesplai.basedatos.*;
import com.codesplai.basedatos.Keytram;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class TramController {

    public static void get_apiKey_Tam() throws Exception {
        URL url = new URL("https://opendata.tram.cat/connect/token");

        Map<String, Object> params = new LinkedHashMap<>();
        params.put("grant_type", "client_credentials");
        params.put("client_id", "miguelangelmasip@gmail.com");
        params.put("client_secret", "a34b89a6-975a-4817-bac0-4b90aff8c535");

        StringBuilder postData = new StringBuilder();

        for (Map.Entry<String, Object> param : params.entrySet()) {
            if (postData.length() != 0)
                postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        conn.setDoOutput(true);
        conn.getOutputStream().write(postDataBytes);

        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
        StringBuilder sb = new StringBuilder();

        for (int c; (c = in.read()) >= 0;) {
            sb.append((char) c);
        }

        String response = sb.toString();
        // System.out.println(response);
        Gson gson = new Gson();
        JsonElement element = gson.fromJson(response, JsonElement.class);
        JsonObject jsonObj = element.getAsJsonObject();
        Keytram kt = new Keytram();
        kt.token = jsonObj.get("access_token").toString();
        kt.token=kt.token.split("\"")[1];
        // System.out.println(kt.token+"xxxxxxxxxxxxxxxxxxxxxxx");
        Date fechatoken = new Date();
        kt.created = fechatoken;
        KeytramController.create(kt);
    }


    public static List<TRAM_stops> get_stops_info() throws Exception {
        List<TRAM_stops> ts = new ArrayList<TRAM_stops>();
        // public static void get_stops_info() throws Exception {
        KeytramController.validacion();
        URL url = new URL("https://opendata.tram.cat//api/v1/stops?pageSize=1000");
        // Map<String, Object> params = new LinkedHashMap<>();
        // params.put("pageSize", 1000);

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        // conn.setRequestProperty("Content-Length",
        // String.valueOf(getDataBytes.length));
  
            //  conn.setRequestProperty("Authorization",
            // "Bearer CfDJ8Ni8WlDP7I5JqnEHVPcXVtUgnV3KdsIjTnAA2E7mnFgWhijIXICt8AmInHYvtNb6MMpiNvLtmZzaquyoeTvNiBM1ZL2MAoUZqn8s5WvFZ-qwNDBW17daIGdcg-4VYeDC9IAR8Y1KX1KyBmt-iHdIVqH9iBzGmciD-YLins1EzbkuX1xi8exSrOdBSMmvKgTUwkRo2x1WwmcRFlhFFWvysLeSbGZYIJ4xNOhm1PkauM7ynHc9PBf8_-_EP8vCyR0EMzSW18x1NiVBAb_Nz9wNEIFwlYDTbTbCnJM6HR5zRlwbogMzOHgJYktyZpH_qnx-BPyrHklAqH89O8Xo9swpSI-iNlJNcUdUrjuRWux4LS2a5uCPej6GEjelW9fk7gjhtgtBfo_fQ7oQv2pKLyUYbf29C7z8EW063hoBNyMzR6lbIhQe2OH54aojRG-pkFo-oR8sJy5C9xWh3ro6mNvtumcov-5RHIGbOxj3ooEmWSMq0EJ4_O3ry3o2ur9-NTVlUvaozgRoTcddLlL00v6_LNv6xIcYSq3kWUCeuLdUFJkSGFUAuyetpaYSnYkxSDm74lZus4FaoeE9rcA6nJgITJOuJ24jDMnPTKPX2rqOr5K1");
             String token = "Bearer "+KeytramController.tokenultimo();
          
            conn.setRequestProperty("Authorization",
               token);
            
                conn.setDoInput(true);

        InputStream is = conn.getInputStream();
        BufferedInputStream bis = new BufferedInputStream(is);
        String strFileContents = "";
        try {

            byte[] contents = new byte[1024];

            int bytesRead = 0;

            while ((bytesRead = bis.read(contents)) != -1) {
                strFileContents += new String(contents, 0, bytesRead);
            }
            Gson gson = new Gson();
            JsonElement element = gson.fromJson(strFileContents, JsonElement.class);
            if (!(element.isJsonNull())) {
            
                JsonArray arrayElementos = element.getAsJsonArray();
    
                for(JsonElement r: arrayElementos){
            
                    JsonObject jsonObj = r.getAsJsonObject();
    
                    TRAM_stops kt = new TRAM_stops();
                    kt.id= Integer.parseInt(jsonObj.get("id").toString());
                    kt.name = jsonObj.get("name").toString();
                    // kt.name=kt.name.split("\"")[1];
                    kt.latitude = jsonObj.get("latitude").toString();
                    kt.longitude = jsonObj.get("longitude").toString();
                    kt.name = jsonObj.get("name").toString();
                    kt.name=kt.name.split("\"")[1];
                    kt.outboundCode= Integer.parseInt(jsonObj.get("outboundCode").toString());
                    kt.returnCode= Integer.parseInt(jsonObj.get("returnCode").toString());
                    kt.image = jsonObj.get("image").toString();
                    // if (kt.image!=null){
                    // kt.image=kt.image.split("\"")[1];} 
                    // else { kt.image= kt.image;}
    
                    kt.gtfsCode = jsonObj.get("gtfsCode").toString();
                    ts.add(kt);
                    
                  
                    // System.out.println(kt);
                }
            }

        } catch (Exception e) {
            System.out.println(e.toString());

        }
        // System.out.println(ts);
        return ts;
    }




    public static List<TRAM_lines> get_lines_info() throws Exception {
        List<TRAM_lines> tsl = new ArrayList<TRAM_lines>();
        // public static void get_stops_info() throws Exception {
        KeytramController.validacion();
        URL url = new URL("https://opendata.tram.cat//api/v1/lines?pageSize=1000");


        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
  
             String token = "Bearer "+KeytramController.tokenultimo();
          
            conn.setRequestProperty("Authorization",
               token);
            
                conn.setDoInput(true);

        InputStream is = conn.getInputStream();
        BufferedInputStream bis = new BufferedInputStream(is);
        String strFileContents = "";
        try {

            byte[] contents = new byte[1024];

            int bytesRead = 0;

            while ((bytesRead = bis.read(contents)) != -1) {
                strFileContents += new String(contents, 0, bytesRead);
            }
            Gson gson = new Gson();
            JsonElement element = gson.fromJson(strFileContents, JsonElement.class);
            if (!(element.isJsonNull())) {
                JsonArray arrayElementos = element.getAsJsonArray();

                for(JsonElement r: arrayElementos){
            
                    JsonObject jsonObj = r.getAsJsonObject();
                    TRAM_lines ktl = new TRAM_lines();
                    ktl.id= Integer.parseInt(jsonObj.get("id").toString());
                    ktl.name = jsonObj.get("name").toString();
                    ktl.name=ktl.name.split("\"")[1];
                    ktl.code= Integer.parseInt(jsonObj.get("code").toString());
                    ktl.id= Integer.parseInt(jsonObj.get("id").toString());
                    ktl.image = jsonObj.get("image").toString();
                    // ktl.image=ktl.image.split("\"")[1];

                    tsl.add(ktl);
                    
                }
           
              
                // System.out.println(kt);
            }

        } catch (Exception e) {
            System.out.println(e.toString());


        }
        // System.out.println(ts);
        return tsl;
    }
}

