package com.codesplai.peticiones;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.Gson;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class BiciboxController {

    public static List<Bicibox> getAll() throws IOException {

        List<Bicibox> bbx = new ArrayList<Bicibox>();

        System.setProperty("http.proxyHost", "...");
        System.setProperty("http.proxyPort", "8080");
        URL url = new URL("http://52.28.193.244/DesktopModules/BiciboxSVC/Bicibox.svc/Station/list");
        URLConnection conn = url.openConnection();
        InputStream is = conn.getInputStream();
        BufferedInputStream bis = new BufferedInputStream(is);
        String strFileContents = "";
        try {

            byte[] contents = new byte[1024];

            int bytesRead = 0;

            while ((bytesRead = bis.read(contents)) != -1) {
                strFileContents += new String(contents, 0, bytesRead);
            }
            Gson gson = new Gson();
            JsonElement element = gson.fromJson(strFileContents, JsonElement.class);
            JsonObject jsonObj = element.getAsJsonObject();

            if (!(element.isJsonNull()) && jsonObj.get("stations")!=null) {

                JsonArray arrayElementos = jsonObj.getAsJsonArray("stations");
             
                for(JsonElement e: arrayElementos){
                    Bicibox bic  = new Bicibox();
                    JsonObject obj = e.getAsJsonObject();
                    bic.buits= Integer.parseInt(obj.get("buits").toString());
                    bic.direccio= obj.get("direccio").toString();
                    bic.direccio=bic.direccio.split("\"")[1];
                    bic.distancia= Integer.parseInt(obj.get("distancia").toString());
                    bic.id= Integer.parseInt(obj.get("id").toString());
                    bic.lat= obj.get("lat").toString();
                    bic.longitud = obj.get("long").toString();
                    bic.municipi= obj.get("municipi").toString();
                    bic.nom= obj.get("nom").toString();
                    bic.plens= Integer.parseInt(obj.get("plens").toString());
                    bbx.add(bic);
                }
            }

        } catch (Exception e) {

            System.out.println(e.toString());
        }
        
        return bbx;
    }

}