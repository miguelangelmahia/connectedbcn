package com.codesplai.servletmysql;

import com.codesplai.basedatos.*;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;

public class Aplications extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        // out.println("Peticion doGet...");
        String app_key = request.getParameter("app_key");
        String api_key = request.getParameter("api_key");
        String option = request.getParameter("option");
        if (api_key != null && app_key != null) {
            if (AppsController.comprovate_credentials(app_key, api_key)) {
                if (option.equals("add")) {
                    String name_app = request.getParameter("nombre");
                    String email = request.getParameter("email");

                    // System.out.println(name_app+" >>> "+email);

                    Usuarios us = UsuariosController.getUsuarioByEmail(email);

                    String apikeyy = UsuariosController.generadorApiKey();
                    boolean existeApiKey = false;

                    do {
                        apikeyy = UsuariosController.generadorApiKey();
                        existeApiKey = UsuariosController.existeApiKey(apikeyy);
                    } while (existeApiKey != false);
                    Apps ap = new Apps(us.id, name_app, apikeyy);
                    UsuariosController.insertApiKey(ap, us);

                    out.println("SUCCESFULL");
                } else if (option.equals("del")) {
                    String id_app = request.getParameter("id");
                    String id_user = request.getParameter("id_user");
                    int id = Integer.parseInt(id_app);
                    AppsController.delete(id);
                    out.println("SUCCESFULL");
                } else {
                    out.println("option incorrecta...");
                }
            } else {
                out.println("ERROR 400");
            }
        } else {
            out.println("ERROR 399");
        }
    }

}

/*
 * 
 * <servlet> <servlet-name> apiRest </servlet-name> <servlet-class>
 * com.codesplai.servletmysql.Api </servlet-class> </servlet>
 * 
 * <servlet-mapping> <servlet-name> apiRest </servlet-name> <url-pattern>
 * /api</url-pattern> </servlet-mapping>
 * 
 */