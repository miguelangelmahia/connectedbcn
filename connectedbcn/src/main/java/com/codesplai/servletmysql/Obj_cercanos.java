package com.codesplai.servletmysql;

import com.codesplai.peticiones.*;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.*;

public class Obj_cercanos {

    public List<Bicibox> List_bicibox = new ArrayList<Bicibox>();
    public List<Stationsbicing> List_stationsbicing = new ArrayList<Stationsbicing>();
    public List<Estaciones> List_metro = new ArrayList<Estaciones>();
    public List<Renfe> List_renfe = new ArrayList<Renfe>();
    public List<TRAM_stops> List_tram = new ArrayList<TRAM_stops>();

    public String to_json() {
        String bicing = new Gson().toJson(this.List_stationsbicing);
        String bicibox = new Gson().toJson(this.List_bicibox);
        String metro = new Gson().toJson(this.List_metro);
        String renfe = new Gson().toJson(this.List_renfe);
        String tram = new Gson().toJson(this.List_tram);
        return "{ \"bicing\": " + bicing + ", \"bicibox\": " + bicibox + ", \"metro\": " + metro + ", \"renfe\": " + renfe+ ", \"tram\": " + tram + " }";
    }
}
