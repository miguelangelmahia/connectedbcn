package com.codesplai.servletmysql;
import java.util.Enumeration;
import java.util.Map;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;


public class HolaMundo extends HttpServlet {
   
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        PrintWriter out = response.getWriter();
        out.println("Peticion doGet...");
        
		out.println("=== Paths ===\n");
		out.println("Request URL : " + request.getRequestURL());
		out.println("Request URI : " + request.getRequestURI());
		out.println("Servlet path : " + request.getServletPath());
		
		out.println("\n=== Headers ===\n");
		Enumeration<String> e = request.getHeaderNames();
		while(e.hasMoreElements()){
			String param = (String) e.nextElement();
			out.println(param + " : " + request.getHeader(param));
		}
		
		out.println("\n=== Parameters ===\n");
		Map<String, String[]> paramsMap = request.getParameterMap();
		for (String key : paramsMap.keySet()) {
			out.println(key + " : " + request.getParameter(key));
		}
		
		out.println("\n=== Session ===\n");
		// returns 0:0:0:0:0:0:0:1 if executed from localhost
		out.println("Client IP address : " + request.getRemoteAddr());
		out.println("Session ID : " + request.getRequestedSessionId());
		// Cookie objects the client sent with this request
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				out.print(cookie.getName() + ";");
			}
		}
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		String user = request.getParameter("user");
		
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		
		// create HTML response
		PrintWriter writer = response.getWriter();
		writer.append("<!DOCTYPE html>\r\n")
			  .append("<html>\r\n")
			  .append("		<head>\r\n")
			  .append("			<title>Welcome message</title>\r\n")
			  .append("		</head>\r\n")
			  .append("		<body>\r\n");
		if (user != null && !user.trim().isEmpty()) {
			writer.append("	Welcome " + user + ".\r\n");
			writer.append("	You successfully completed this javatutorial.net example.\r\n");
		} else {
			writer.append("	You did not entered a name!\r\n");
		}
		writer.append("		</body>\r\n")
			  .append("</html>\r\n");
	}	
	

}