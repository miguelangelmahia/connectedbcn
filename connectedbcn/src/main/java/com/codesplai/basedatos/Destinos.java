package com.codesplai.basedatos;

public class Destinos {
    int id_destinos;
    String nombre;
    String latitud;
    String longitud;
    int usuarios_id_usuario;

    public Destinos(int id, String nombre, String latitud, String longitud, int usuariosid) {
        this.id_destinos = id;
        this.nombre = nombre;
        this.latitud = latitud;
        this.longitud = longitud;
        this.usuarios_id_usuario = usuariosid;

    }

    @Override
    public String toString() {
        return String.format("id:%d nombre:%s latitud:%s longitud:%s usuarios_id_usuario%d",
         this.id_destinos, this.nombre, this.latitud, this.longitud, this.usuarios_id_usuario);
    }
}