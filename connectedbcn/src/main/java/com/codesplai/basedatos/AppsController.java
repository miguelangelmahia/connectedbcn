package com.codesplai.basedatos;

import java.util.List;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;


import com.mysql.jdbc.Connection;

public class AppsController {
    private static final String TABLE = "apps";
    private static final String KEY1 = "id";

    public static List<Apps> getAll() {

        List<Apps> listaApps = new ArrayList<Apps>();
        String sql = String.format("select * from %s;", TABLE);

        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Apps ap = new Apps(rs.getInt("id_usuario"), rs.getInt("id"), rs.getString("nombre"),
                        rs.getString("apikey"));
                listaApps.add(ap);
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return listaApps;
    }

    public static void create(Apps ap) {

        String sql = "INSERT INTO apps (id_usuario, id, nombre, apikey) VALUES (?, NULL, ?, ?);";
        try (Connection conn = DBConn.getConn(); PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, ap.id_usuario);
            pstmt.setInt(2, ap.id);
            pstmt.setString(3, ap.nombre);
            pstmt.setString(4, ap.apikey);
            pstmt.executeUpdate();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void delete(int id) {
        String sql = String.format("DELETE FROM %s WHERE %s.%s=%d", TABLE, TABLE, KEY1, id);
        System.out.println(sql);

        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

            stmt.executeUpdate(sql);

        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
    }

    // COMPROBACION APP_KEY Y API_KEY
    public static boolean comprovate_credentials(String app, String api){
        // System.out.println(app + "|" + api);
        List<Apps> list = getAll();
        for(Apps ap : list){
            // System.out.println(ap.nombre+ "|" +app);
            if(ap.nombre.equals(app)){
                // System.out.println(ap.apikey+ "|" +api);
                if(ap.apikey.equals(api)){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }
        return false;
    }

    // RETURN ID
    public static int get_id_por_parametros(String app, String api){
        int result = 0;
        List<Apps> list = getAll();
        for(Apps ap : list){
            if(ap.nombre.equals(app)){
                if(ap.apikey.equals(api)){
                    result = ap.id;
                }
            }
        }
        return result;
    }


    public static List<Apps> getAll_user(Usuarios us) {

        List<Apps> listaApps = new ArrayList<Apps>();
        String sql = String.format("select * from %s WHERE id_usuario=%d", TABLE,us.id);

        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Apps ap = new Apps(rs.getInt("id_usuario"), rs.getInt("id"), rs.getString("nombre"),
                        rs.getString("apikey"));
                listaApps.add(ap);
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return listaApps;
    }

}