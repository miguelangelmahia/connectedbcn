package com.codesplai.basedatos;
import java.util.*;


public class Keytram {
    public int id;
    public Date created;
    public String token;
    public Date expired;


    // constructor vacio
    public Keytram() {

    }
// constructor token
public Keytram(String token) {
    this.token = token;

}
    // constructor completo
    public Keytram(int id, Date created,  String token, Date expired) {
        this.id = id;
        this.created = created;
        this.token = token;
        this.expired = expired;
    }

    @Override
    public String toString() {
        return String.format("id:%d created:%s token:%s expired:%s ", this.id,
                this.id, this.created, this.token, this.expired);
    }

}