package com.codesplai.basedatos;

import java.util.List;
import java.util.Random;
import java.util.ArrayList;
import java.util.Date;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.sql.ResultSet;

import com.mysql.jdbc.Connection;

public class UsuariosController {
	private static final String TABLE = "usuarios";
	private static final String KEY1 = "id_usuario";

	// getAll devuelve todos los registros de la tabla
	public static List<Usuarios> getAll() {

		List<Usuarios> listaUsuarios = new ArrayList<Usuarios>();
		String sql = String.format("select * from %s;", TABLE);
		// String sql = "select id,nombre,password from "+TABLE;

		try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				Usuarios u = new Usuarios(rs.getInt("id_usuario"), rs.getString("email"), rs.getString("user"),
						rs.getString("password"), rs.getString("telefono"), rs.getDate("nacimiento"),
						rs.getString("urlfoto"));
				listaUsuarios.add(u);
			}
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		return listaUsuarios;

	}

	public static Date stringToDate(String sdata) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date theDate = null;
		try {
			theDate = df.parse(sdata);
		} catch (ParseException e) {
			// e.printStackTrace();
		}
		return theDate;
	}

	public static void updateContrasena(String email, String contrasena) {
		String sql = "UPDATE usuarios SET password = ? WHERE email = ?;";
		try (Connection conn = DBConn.getConn(); PreparedStatement pstmt = conn.prepareStatement(sql)) {

			pstmt.setString(1, contrasena);
			pstmt.setString(2, email);
			pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static void updateDesarrollador(String email, int desarrollador) {
		String sql = "UPDATE usuarios SET developer = ? WHERE email = ?;";
		try (Connection conn = DBConn.getConn(); PreparedStatement pstmt = conn.prepareStatement(sql)) {

			pstmt.setInt(1, desarrollador);
			pstmt.setString(2, email);
			pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static void updateTelefono(String email, String telefono) {
		String sql = "UPDATE usuarios SET telefono = ? WHERE email = ?;";
		try (Connection conn = DBConn.getConn(); PreparedStatement pstmt = conn.prepareStatement(sql)) {

			pstmt.setString(1, telefono);
			pstmt.setString(2, email);
			pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static void updateFechaNacimiento(String email, String fechaNacimiento) {
		String sql = "UPDATE usuarios SET nacimiento = ? WHERE email = ?;";
		try (Connection conn = DBConn.getConn(); PreparedStatement pstmt = conn.prepareStatement(sql)) {
			Date fechaFormateada = stringToDate(fechaNacimiento);
			long ms = fechaFormateada.getTime();
			java.sql.Date fecha_sql = new java.sql.Date(ms);
			pstmt.setDate(1, fecha_sql);
			pstmt.setString(2, email);
			pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static String insertUsuario(Usuarios us) {

		String error = "";

		if (existeUsuario(us.getEmail())) {
			error = "Ya existe un usuario registrado con ese email";
			return error;
		}

		String sql = "INSERT INTO usuarios ( email, user, password) VALUES ( ?, ?, ?);";
		try (Connection conn = DBConn.getConn(); PreparedStatement pstmt = conn.prepareStatement(sql)) {

			pstmt.setString(1, us.email);
			pstmt.setString(2, us.user);
			pstmt.setString(3, us.password);
			pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return error;
	}

	public static boolean existeUsuario(String email) {
		String sql = "SELECT * FROM usuarios where email ='" + email + "'";
		boolean existe = false;
		try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {
			ResultSet rs = stmt.executeQuery(sql);
			if (rs.next()) {
				existe = true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return existe;
	}

	public static boolean validarInicioSesion(String email, String contrasena) {
		String sql = "SELECT * FROM usuarios where email ='" + email + "' AND password ='" + contrasena + "'";
		boolean existe = false;
		try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {
			ResultSet rs = stmt.executeQuery(sql);
			if (rs.next()) {
				existe = true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return existe;
	}
	
	public static void insertApiKey(Apps ap, Usuarios us) {
		String sql = "INSERT INTO apps ( id_usuario, nombre, apikey) VALUES ( ?, ?, ?);";
		try (Connection conn = DBConn.getConn(); PreparedStatement pstmt = conn.prepareStatement(sql)) {

			pstmt.setInt(1, us.id);
			pstmt.setString(2, ap.nombre);
			pstmt.setString(3, ap.apikey);
			pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static void updateApiKey(String apikey, Usuarios us) {
		String sql = "UPDATE apps SET apikey = ? WHERE id_usuario = ?;";
		try (Connection conn = DBConn.getConn(); PreparedStatement pstmt = conn.prepareStatement(sql)) {

			pstmt.setString(1, apikey);
			pstmt.setInt(2, us.id);
			pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static boolean existeApiKey(String apikey) {
		String sql = "SELECT * FROM apps where apikey ='" + apikey + "'";
		boolean existe = false;
		try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {
			ResultSet rs = stmt.executeQuery(sql);
			if (rs.next()) {
				existe = true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return existe;
	}

	public static String generadorApiKey() {
		String apikey = "";
		String caracteres = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		Random random = new Random();

		while (!apikey.matches("[a-z]+.*[A-Z]+.*\\d+.*")) {
			for (int i = 0; i < 15; i++) {
				int randomCaracteres = random.nextInt(caracteres.length());
				apikey += caracteres.charAt(randomCaracteres);
			}
			if (!apikey.matches("[a-z]+.*[A-Z]+.*\\d+.*")) {
				apikey = "";
			}
		}
		return apikey;
	}

	public static Usuarios getUsuarioByEmail(String email) {
		String sql = "SELECT * FROM usuarios where email ='" + email + "'";
		Usuarios us = new Usuarios();
		try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {
			ResultSet rs = stmt.executeQuery(sql);
			if (rs.next()) {
				us.setId(rs.getInt("id_usuario"));
				us.setEmail(rs.getString("email"));
				us.setUser(rs.getString("user"));
				us.setPassword(rs.getString("password"));
				us.setTelefono(rs.getString("telefono"));
				us.setNacimiento(rs.getDate("nacimiento"));
				boolean develop =  rs.getBoolean("developer");
				System.out.println(develop);
				us.setDeveloper(develop);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return us;
	}

	// A�ADIR USUARIO A UNA TABLA
	public static void create(Usuarios us) {
		// INSERT INTO `usuarios` (`id_usuario`, `email`, `user`, `password`,
		// `telefono`, `nacimiento`, `urlfoto`, `favorito`)
		// VALUES (NULL, 'jks@gmail.com', 'juanka', '123456', '999-999-999',
		// '2018-08-22', NULL, NULL);
		String sql = "INSERT INTO usuarios (id_usuario, email, user, password, telefono, nacimiento, urlfoto, developer) VALUES (NULL, ?, ?, ?, ?, NULL, NULL, NULL);";
		try (Connection conn = DBConn.getConn(); PreparedStatement pstmt = conn.prepareStatement(sql)) {

			pstmt.setString(1, us.email);
			pstmt.setString(2, us.user);
			pstmt.setString(3, us.password);
			pstmt.setString(4, us.telefono);
			pstmt.setBoolean(5, us.developer);
			pstmt.executeUpdate();

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	// BORRA USUARIO
	public static void delete_Usuario(int id) {

		// DELETE FROM `Usuarios` WHERE `Usuarios`.`id_usuarios` = ?"
		String sql = String.format("DELETE FROM %s WHERE %s.%s=%d", TABLE, TABLE, KEY1, id);
		System.out.println(sql);

		try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

			stmt.executeUpdate(sql);

		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
	}

	// ACTUALIZA USUARIO de(id,email,user,passwrod,telefono) con id=x
	public static void update_Usuario(Usuarios us) {
		// UPDATE usuarios SET email = 'mireia@gmail.es', password = '4331', telefono =
		// '888-888-848' WHERE usuarios.id_usuario = 2;
		// UPDATE usuarios SET nombre = "TC_10" WHERE idUsuarios = 11;

		String sql = "UPDATE usuarios SET email = ?, user = ?, password = ?, telefono = ? WHERE usuarios.id_usuario = ?;";
		System.out.println(sql);

		try (Connection conn = DBConn.getConn(); PreparedStatement pstmt = conn.prepareStatement(sql)) {

			pstmt.setString(1, us.email);
			pstmt.setString(2, us.user);
			pstmt.setString(3, us.password);
			pstmt.setString(4, us.telefono);
			pstmt.setInt(5, us.id);
			pstmt.executeUpdate();

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}