package com.codesplai.basedatos;

import java.util.Date;

public class Usuarios {

    public int id;
    public String email;
    public String user;
    public String password;
    public String telefono;
    public Date nacimiento;
    public String urlFoto;
    public Boolean developer;
    
    public Usuarios() {
        
    }
    
    public Usuarios(String mail,String u, String pass) {
        
        this.email = mail;
        this.user = u;
        this.password = pass;
    }


    public Usuarios(String mail,String u, String pass, String tel) {
        
        this.email = mail;
        this.user = u;
        this.password = pass;
        this.telefono = tel;   
    }
    public Usuarios(int d,String mail,String u, String pass, String tel) {
        this.id = d;
        this.email = mail;
        this.user = u;
        this.password = pass;
        this.telefono = tel;
                
    }
    public Usuarios(int d,String mail,String u, String pass, String tel, Date naci, String urFot) {
        this.id = d;
        this.email = mail;
        this.user = u;
        this.password = pass;
        this.telefono = tel;
        this.nacimiento = naci;
        this.urlFoto = urFot;
        
    }

    public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getUser() {
		return user;
	}


	public void setUser(String user) {
		this.user = user;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getTelefono() {
		return telefono;
	}


	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}


	public Date getNacimiento() {
		return nacimiento;
	}


	public void setNacimiento(Date nacimiento) {
		this.nacimiento = nacimiento;
	}


	public String getUrlFoto() {
		return urlFoto;
	}


	public void setUrlFoto(String urlFoto) {
		this.urlFoto = urlFoto;
	}


	public Boolean getDeveloper() {
		return developer;
	}


	public void setDeveloper(Boolean developer) {
		this.developer = developer;
	}

	@Override
	public String toString() {
		return "Usuarios [id=" + id + ", email=" + email + ", user=" + user + ", password=" + password + ", telefono="
				+ telefono + ", nacimiento=" + nacimiento + ", urlFoto=" + urlFoto + ", developer=" + developer + "]";
	}

	
}