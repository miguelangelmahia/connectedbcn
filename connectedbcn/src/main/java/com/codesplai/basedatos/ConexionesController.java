package com.codesplai.basedatos;

import java.util.List;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;

import com.mysql.jdbc.Connection;

public class ConexionesController {
    private static final String TABLE = "conecxiones";
    private static final String KEY1 = "id";

    public static List<Conexiones> getAll() {
        
        List<Conexiones> listaConexiones = new ArrayList<Conexiones>();
        String sql = String.format("select * from %s;", TABLE);


        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Conexiones cn = new Conexiones(rs.getInt("id_apps"), rs.getInt("id"), rs.getString("host"), rs.getString("maquina"), rs.getString("idsesion"),
                rs.getString("llamada"),rs.getString("type"), rs.getString("parametros"));
                listaConexiones.add(cn);
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return listaConexiones;
    }
    
    public static void create(Conexiones cn) {

        String sql = "INSERT INTO conecxiones (id_apps, id, host, maquina, idsesion, llamada, type, parametros) VALUES (?, NULL, ?, ?, ?, ?, ?, ?);";
        try (Connection conn = DBConn.getConn(); PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, cn.id_apps);       
            pstmt.setString(2, cn.host);
            pstmt.setString(3, cn.maquina);
            pstmt.setString(4, cn.idsesion);
            pstmt.setString(5, cn.llamada);
            pstmt.setString(6, cn.type);
            pstmt.setString(7, cn.parametros);

            pstmt.executeUpdate();
            System.out.println("Saved: " + cn.toString());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public static void delete(int id) {
        String sql = String.format("DELETE FROM %s WHERE %s.%s=%d", TABLE, TABLE, KEY1, id);
        System.out.println(sql);

        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

            stmt.executeUpdate(sql);

        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
    }
      
}