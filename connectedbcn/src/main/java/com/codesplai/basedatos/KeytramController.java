package com.codesplai.basedatos;

import com.codesplai.peticiones.*;

import java.util.List;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.Date;
import java.sql.Timestamp;
import com.mysql.jdbc.Connection;

public class KeytramController {
    private static final String TABLE = "keytram";
    private static final String KEY1 = "id";

    public static List<Keytram> getAll() {

        List<Keytram> listaKeytram = new ArrayList<Keytram>();
        String sql = String.format("select * from %s;", TABLE);

        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Keytram kt = new Keytram(rs.getInt("id"), rs.getDate("created"), rs.getString("token"),
                        rs.getDate("expired"));
                listaKeytram.add(kt);
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return listaKeytram;
    }

    public static void create(Keytram ktm) {

        String sql = "INSERT INTO keytram (id, created, token, expired) VALUES (NULL, ?, ?, ?);";
        try (Connection conn = DBConn.getConn(); PreparedStatement pstmt = conn.prepareStatement(sql)) {

            Long segundos = ktm.created.getTime();
            java.sql.Date fecha = new java.sql.Date(segundos);
             pstmt.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
            pstmt.setString(2, ktm.token);
            pstmt.setDate(3, fecha);

            pstmt.executeUpdate();
            // System.out.println(ktm.toString());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void delete(int id) {
        String sql = String.format("DELETE FROM %s WHERE %s.%s=%d", TABLE, TABLE, KEY1, id);
        System.out.println(sql);

        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

            stmt.executeUpdate(sql);

        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
    }

    public static Keytram idmayor() {
        String sql = String.format("SELECT * FROM connectedbcn.keytram ORDER BY  id DESC LIMIT 0,1");
        // System.out.println(sql);
        Keytram ktm = new Keytram();

        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);
            rs.next();

            Date date = new Date();
            Timestamp timestamp = rs.getTimestamp("created");
            if (timestamp != null)
                date = new Date(timestamp.getTime());


            Keytram kt = new Keytram(rs.getInt("id"), date, rs.getString("token"),
                    rs.getDate("expired"));
            ktm = kt;

        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return ktm;
    }

    public static void validacion() {
        Keytram val = idmayor();
        Date hoy = new Date();
        long milisegundosactuales = hoy.getTime();        
        long milisegundosexpiracion = val.created.getTime();
        // System.out.println(milisegundosactuales);
        // System.out.println(milisegundosexpiracion);

        if (milisegundosactuales - milisegundosexpiracion > 3600000 || val==null) {
            try {
                TramController.get_apiKey_Tam();
            }

            catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            // System.out.println("Sigue vigente");
        }
    }



    public static String tokenultimo() {
        String sql = String.format("SELECT token FROM connectedbcn.keytram ORDER BY  id DESC LIMIT 0,1");
        // System.out.println(sql);
        Keytram ktm = new Keytram();
        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);
            rs.next();
            Keytram kt = new Keytram(rs.getString("token"));
            ktm = kt;
            // System.out.println(ktm.token);
 

        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return ktm.token;

    }
}